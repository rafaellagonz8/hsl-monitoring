package meteoconsult.products.hsl;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author <a href="hoang281283@gmail.com">Minh Hoang TO</a>
 * @date 11/18/12
 * 
 * @Test
 */
public class LinkedHashMapTest {

	public static void main(String[] args) throws Exception {
		// Expect to have a map whose size never exceed 100
		final LinkedHashMap map = new LinkedHashMap() {
			@Override
			protected boolean removeEldestEntry(Map.Entry eldest) {
				return size() > 100;
			}
		};
		
		final Map syncMap = Collections.synchronizedMap(new LinkedHashMap() {
			@Override
			protected boolean removeEldestEntry(Map.Entry eldest) {
				return size() > 100;
			}
		});
		

		final AtomicInteger counter = new AtomicInteger(0);

		final String blah = "BlahBlah";

		Thread t1 = new Thread() {
			@Override
			public void run() {
				for (int i = counter.incrementAndGet(); i < 1000; i = counter
						.incrementAndGet()) {
					map.put(i, blah);
				}
			}
		};

		Thread t2 = new Thread() {
			@Override
			public void run() {
				for (int i = counter.incrementAndGet(); i < 1000; i = counter
						.incrementAndGet()) {
					map.put(i, blah);
				}
			}
		};
		
		final AtomicInteger syncCounter = new AtomicInteger(0);
		
		Thread t3 = new Thread() {
			@Override
			public void run() {
				for (int i = syncCounter.incrementAndGet(); i < 1000; i = syncCounter.incrementAndGet()) {
					syncMap.put(i, blah);
				}
			}
		};

		Thread t4 = new Thread() {
			@Override
			public void run() {
				for (int i = syncCounter.incrementAndGet(); i < 1000; i = syncCounter.incrementAndGet()) {
					syncMap.put(i, blah);
				}
			}
		};

		t1.start();
		t2.start();
		t1.join();
		t2.join();

		System.out.println("Current size of linked hash map is: " + map.size());
		
		
		t3.start();
		t4.start();
		t3.join();
		t4.join();

		System.out.println("Current size of synced linked hash map is: " + syncMap.size());
	}
}
