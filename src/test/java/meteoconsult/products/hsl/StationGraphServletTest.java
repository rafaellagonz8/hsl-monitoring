package meteoconsult.products.hsl;

import static org.junit.Assert.assertTrue;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Random;
import java.util.TimeZone;
import java.util.concurrent.atomic.AtomicInteger;

import meteoconsult.database.tables.Table;
import meteoconsult.database.tables.meteo.Mg_forecast;
import meteoconsult.database.tables.meteo.Plc_obs_rtm;
import meteoconsult.products.hsl.chart.HslWindChart;

import org.junit.BeforeClass;
import org.junit.Test;

import ChartDirector.XYChart;

public class StationGraphServletTest {
	
	final static Table[] tables = new Table[10];
	static {
		Mg_forecast mf1 = new Mg_forecast();
		mf1.getForecast(6040,24);
		tables[0] = mf1;
		Plc_obs_rtm pl2 = new Plc_obs_rtm();
		pl2.getLatestObservation(1001);
		tables[1] = pl2;
		Mg_forecast mf3 = new Mg_forecast();
		mf3.getForecast(1001,36);
		tables[2] = mf3;
		Mg_forecast mf4 = new Mg_forecast();
		mf4.getForecast(1002,48);
		tables[3] = mf4;
		Mg_forecast mf5 = new Mg_forecast();
		mf5.getForecast(1003,24);
		tables[4] = mf5;
		
		Mg_forecast mf6 = new Mg_forecast();
		mf5.getForecast(1004,24);
		tables[5] = mf6;
		
		Mg_forecast mf7 = new Mg_forecast();
		mf7.getForecast(1005,24);
		tables[6] = mf7;
		
		Mg_forecast mf8 = new Mg_forecast();
		mf5.getForecast(1006,24);
		tables[7] = mf8;
		
		Mg_forecast mf9 = new Mg_forecast();
		mf5.getForecast(1007,24);
		tables[8] = mf9;
		
		Mg_forecast mf10 = new Mg_forecast();
		mf10.getForecast(1008,24);
		tables[9] = mf10;
		
		
		
		
	}
	
	final static XYChart[] charts = new XYChart[5];
	static {
		charts[0] =  new HslWindChart(100, 200, TimeZone.getDefault(), null);
		//System.out.println(Runtime.getRuntime().freeMemory());
		charts[1] = new XYChart(100, 201);
		charts[2] = new XYChart(100, 202);
		//System.out.println(Runtime.getRuntime().freeMemory());
		charts[3] = new HslWindChart(100, 203, TimeZone.getDefault(), null);
		charts[4] = new XYChart(100, 204);
	}

	/* cache the used charts, to prevent threading problems */
	private static LinkedHashMap<Table,XYChart> cacheMap  = new LinkedHashMap<Table,XYChart>(3) {
		@Override
		protected boolean removeEldestEntry(Map.Entry<Table,XYChart> eldest) {
			return size() > 3;
		}
		
		
		@Override
		public XYChart put(Table table ,XYChart chart) {	
			XYChart oldVal =  super.put(table, chart);
			System.out.println("put: "+table.hashCode() +" =>  "+chart.hashCode()+"    old: "+(oldVal==null?"null":oldVal.hashCode())+"    sz: "+size());
			return oldVal;
		}
	};
	
	private static XYChart testChart = new XYChart(100, 202);
	

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		System.out.println(Runtime.getRuntime().freeMemory());
		Mg_forecast mf1 = new Mg_forecast();
		mf1.getForecast(6040,24);
		//Mg_forecast mf2 = new Mg_forecast();
		Plc_obs_rtm pl2 = new Plc_obs_rtm();
		pl2.getLatestObservation(1001);
		Mg_forecast mf3 = new Mg_forecast();
		mf3.getForecast(1001,36);
		Mg_forecast mf4 = new Mg_forecast();
		mf4.getForecast(1002,48);
		Mg_forecast mf5 = new Mg_forecast();
		mf5.getForecast(1003,24);
		
		cacheMap.put(mf1, new HslWindChart(100, 200, TimeZone.getDefault(), null));
		//System.out.println(Runtime.getRuntime().freeMemory());
		cacheMap.put(pl2, new XYChart(100, 201));
		cacheMap.put(mf3, testChart);
		//System.out.println(Runtime.getRuntime().freeMemory());
		cacheMap.put(mf4, new HslWindChart(100, 203, TimeZone.getDefault(), null));
		cacheMap.put(mf5, new XYChart(100, 204));
		//System.out.println(Runtime.getRuntime().freeMemory());
	}

	
	
	@Test
	public void sizeIsThree() {
		System.out.println("cacheMap.sz: "+cacheMap.size());
		assertTrue("oldest enries should be removed",cacheMap.size()==3);
//		XYChart firstVal = cacheMap.values().iterator().next();
	}
	
	@Test
	public void firstEntryValIsWhat() {
		XYChart firstVal = cacheMap.values().iterator().next();
		assertTrue("first in map should be testChart",firstVal.equals(testChart));
	}
	
	
	// Expect to have a map whose size never exceed 3
	private static LinkedHashMap<Table,XYChart> notSyncedMap  = new LinkedHashMap<Table,XYChart>(3) {
		@Override
		protected boolean removeEldestEntry(Map.Entry<Table,XYChart> eldest) {
			return size() > 3;
		}
	};
	
	private static Map<Table,XYChart> syncMap = Collections.synchronizedMap(new LinkedHashMap<Table,XYChart>() {
		@Override
		protected boolean removeEldestEntry(Map.Entry<Table,XYChart> eldest) {
			return size() > 3;
		}
	});
	
	@Test
	public void multiThreadTest() {
		System.out.println("-- multithread test --");
		final AtomicInteger counter = new AtomicInteger(0);


		Thread t1 = new Thread() {
			@Override
			public void run() {
				for (int i = counter.incrementAndGet(); i < 5000; i = counter
						.incrementAndGet()) {
					notSyncedMap.put(getRandomTable(), getRandomXYChart());
				}
			}
		};

		Thread t2 = new Thread() {
			@Override
			public void run() {
				for (int i = counter.incrementAndGet(); i < 5000; i = counter
						.incrementAndGet()) {
					notSyncedMap.put(getRandomTable(), getRandomXYChart());
				}
			}
		};
		
		final AtomicInteger syncCounter = new AtomicInteger(0);
		
		Thread t3 = new Thread() {
			@Override
			public void run() {
				for (int i = syncCounter.incrementAndGet(); i < 5000; i = syncCounter.incrementAndGet()) {
					syncMap.put(getRandomTable(), getRandomXYChart());
				}
			}
		};

		Thread t4 = new Thread() {
			@Override
			public void run() {
				for (int i = syncCounter.incrementAndGet(); i < 5000; i = syncCounter.incrementAndGet()) {
					syncMap.put(getRandomTable(), getRandomXYChart());
				}
			}
		};

		t1.start();
		t2.start();
		try {
			t1.join();
			t2.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println("Current size of linked hash map is: " + notSyncedMap.size());
		StringBuffer sbMapKeys = new StringBuffer();
		for (Table key: notSyncedMap.keySet()) {
			sbMapKeys.append(key.hashCode()).append(" ");
		}
		System.out.println("map keys: " +sbMapKeys);
		assertTrue("multithreaded non-synchronized map >= 3",notSyncedMap.size()>=3);
		
		
		t3.start();
		t4.start();
		try {
			t3.join();
			t4.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		System.out.println("Current size of synced linked hash map is: " + syncMap.size());
		StringBuffer sbSyncMapKeys = new StringBuffer();
		for (Table key: syncMap.keySet()) {
			sbSyncMapKeys.append(key.hashCode()).append(" ");
		}
		System.out.println("synced map keys: " +sbSyncMapKeys);
		
		assertTrue("multithreaded synchronized map = 3",cacheMap.size()==3);
	}
	
	
	
	private Table getRandomTable() {
		int rnd = (int)Math.floor( 10.0*Math.random());
		return tables[rnd];
	}
	
	private XYChart getRandomXYChart() {
		int rnd = (int)Math.floor( 5.0*Math.random());
		return charts[rnd];
	}
	

	
	
}
