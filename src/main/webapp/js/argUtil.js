
/**
* Reloads the frame called 'content' and adds the given arg
*/
function reloadContentAddArg(arg,argValue) {
	var location = parent.content.location;
	var newQuery = addArgument(location,arg,argValue);

	var path = location.pathname;
	path=path+newQuery;
	location.href=path; //and automatically reloads
}


/**
* Adds an argument to the url of the location in a proper syntax. 
* Checks for existence of the argument
* @return the new QueryString
*/
function addArgument(location,arg,argValue) {
	var newQuery="";
	//find argument in current queryString
	var query = location.search.substring(1); //get query string
	var argFound=false;
	//if there is a query, split in pairs
	if (query.length>0) {
		var pairs  = query.split("&");
		for (var i=0;i<pairs.length;i++) {
			if (newQuery.length==0) newQuery="?";
			else newQuery+="&";
			
			var pos = pairs[i].indexOf('=');
			if (pos>-1) {
				var argName=pairs[i].substring(0,pos);
				if (argName==arg) {
					newQuery+=arg+"="+unescape(argValue);
					argFound=true;
				}
				else
					newQuery+=pairs[i];
			} //if '=' found
			else
				newQuery+=pairs[i]
		} //for
	} //query.length>0
	//if not yet found then add
	if (!argFound) {
		if (newQuery.length==0) 
			newQuery="?"+arg+"="+unescape(argValue);
		else
			newQuery+="&"+arg+"="+unescape(argValue);
	}
	return newQuery;
}

