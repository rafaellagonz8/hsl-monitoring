var dtgSelecter;
var intervalSelecter;
var elementSelecter;

var markers;
var reloadTimer;

var gmap;

var ZOOM_TRAJECT = 1;
var ZOOM_BRIDGE = 2;
var ZOOM_RADAR = 3;

var zoomType = ZOOM_TRAJECT;

var soundOn = true;

var unknownIcon = {
  path: google.maps.SymbolPath.CIRCLE,
  scale: 3,
  strokeColor: '#FFAA00',
  strokeOpacity: 0.8
};

/**
 *
 */
var initHslMonitor = function (gmap) {
  this.gmap = gmap;
  zoomIn(zoomType);

  dtgSelecter = $("#dtgSelecter");
  intervalSelecter = $("#intervalSelecter");
  elementSelecter = $("#elementSelecter");

  loadDtgs();

  $(dtgSelecter).change(function () {
    var dtg = $(dtgSelecter).val();
    timerChanged(dtg);
  });

  initMarkers(gmap);


}; //init

/** load the DTG's and Radar images */
function loadDtgs() {
  var interval = $(intervalSelecter).val();
  $.ajax({
    url: "dtgServlet?type=dtgs&interval=" + interval,
    dataType: "xml",
    success: function (xml) {
      dtgSelecter.empty();
      var firstValue = null;

      $(xml).find('dtg').each(function () {
        var dtg = this;
        var name = dtg.getAttribute("name");
        var value = dtg.getAttribute("value");
        dtgSelecter.append(
          $('<option></option>').val(value).html(name)
        );
        if (!firstValue) firstValue = value;
      }); //each


      if (!$(dtgSelecter).val()) {
        $(dtgSelecter).val(firstValue);
      };

      timerChanged($(dtgSelecter).val());
    } //success
  });

}


function refreshAll() {
  timerChanged($(dtgSelecter).val());
}

/**
 * The central method which triggers all update events
 */
function timerChanged(dtg) {

  var interval = $(intervalSelecter).val();
  //refresh the datapage
  $.ajax({
    url: "dataProviderServlet?type=refresh&interval=" + interval + "&dtg=" + dtg,
    dataType: "text",
    success: function () {
      //refresh the datapage
      $("#dataBlock").load("dataPage.jsp?dummy=" + new Date().getMilliseconds());
      //refresh the markers
      if (markers != null)
        loadMapData();
      else
        console.info("no markers yet available, loadmapData suspended");
    }, //success
    error: function (xhr, ajaxOptions, thrownError) {
      console.info("not able to refresh the data: xhr: " + xhr + "   ajaxOptions: " + ajaxOptions + "    thrownError: " + thrownError);
    }
  });


  //load new radar image with dtg rounded down to 5 min
  var pos = dtg.indexOf(':');
  if (pos > -1) {
    var minutes = dtg.substring(pos + 1);
    var roundMinutes = Math.floor(minutes / 5) * 5;
    if (roundMinutes < 10) {
      roundMinutes = "0" + roundMinutes;
    }

    var radarDtg = dtg.substring(0, pos) + ":" + roundMinutes;
    $("#dtg").val(radarDtg);
    radar.setDtg(radarDtg);
  } //pos>-1

  //cancel existing timer
  if (reloadTimer) {
    clearTimeout(reloadTimer);
  }
  //do auto reload if the first option is selected
  var index = $(dtgSelecter).prop("selectedIndex");
  if (index === 0) {
    reloadTimer = setTimeout(function () {
      loadDtgs();
    }, 1 * 60000);
  } //index=0, find latest

} //timerchanged

/** logout */
function logout() {
  window.location = "logout.jsp";
}

//----sound methods ----------

function setSoundOn(val, replayFile) {
  soundOn = val;
  if (replayFile) {
    showAlarmSound(replayFile);
  }
}

/**
 * Shows and plays the alarmsound
 * @param wavFile
 */
function showAlarmSound(wavFile) {
  console.info("soundfile: " + wavFile);
  if (wavFile == null || wavFile === "") {
    $("#header_table tr").css("background-color", "#0049B2");
  }
  else {
    $("#header_table tr").css("background-color", "red");
    var soundTag;
    if (soundOn) {
      playSound(wavFile);
      soundTag = "<a href='javascript:setSoundOn(false,\"" + wavFile + "\");'><img title='sound is on' src='images/sound_on.gif'></a>";
    }
    else {
      soundTag = "<a href='javascript:setSoundOn(true,\"" + wavFile + "\");'><img title='sound is off' src='images/sound_off.gif'></a>";
    }
    $("#space_beep").html(soundTag);
  } //else
}

/**
 * Used for playing the test sound
 * @param wavFile
 */
function zzzzplaySound(wavFile) {
  console.info("playsound soundfile: " + wavFile);
  $("#soundTest").html('<embed hidden="true" loop="false" autostart="true" src="' + wavFile + '">');
}


function playSound(f) {
  var audio = document.createElement("audio");
  //audio.src = "wav/ding.mp3";

  $('<source>').attr('src', f + ".mp3").appendTo(audio);
  $('<source>').attr('src', f + ".wav").appendTo(audio);

  audio.addEventListener("ended", function () {
    document.removeChild(this);
  }, false);
  audio.play();
}


//-----marker methods-------
/**
 * locates the markers on the map
 */
function initMarkers(gmap) {
  $.ajax({
    url: "mapDataServlet?type=stationdata",
    cache: false,
    dataType: "xml",
    success: function (data) {
      //first get the flag descriptions
      var flagDescriptions = new Array();
      $(data).find('flag').each(function (index) {
        var id = $(this).attr("id");
        flagDescriptions[id] = new Object();
        flagDescriptions[id].id = id;
        flagDescriptions[id].descr = $(this).attr("descr");
      }); //each flagDescriptions

      //now create the markers for each station
      markers = new Array();
      $(data).find('station').each(function (index) {
        var station = $(this);
        var lat = $(station).attr("lat");
        var lon = $(station).attr("lon");

        //create description from the flag
        var flag = $(station).attr("flag");
        var description = station.attr("name");
        $(flagDescriptions).each(function () {
          //console.info("flag: "+flag+"   check on: "+this.id+"   descr: "+this.descr+"   =and=> "+(flag&this.id)+"   =or=> "+(flag|this.id) );
          if (flag & this.id) {
            description += "\n" + this.descr;
          }
        });

        //create the icon and marker
        var marker = new google.maps.Marker({
          position: new google.maps.LatLng(lat, lon),
          map: gmap,
          title: description,
          icon: unknownIcon
        });
        var statId = station.attr("id");

        markers[statId] = marker;

        //reload the datapage after a click on a marker
        google.maps.event.addListener(marker, 'click', function () {
          $.ajax({
            url: "dataProviderServlet?type=station&id=" + statId,
            cache: false,
            dataType: "text",
            success: function () {
              //refresh the datapage
              $("#dataBlock").load("dataPage.jsp?dummy=" + new Date().getMilliseconds());
            },
            error: function (xhr, ajaxOptions, thrownError) {
              console.info("not able set the station to " + statId + ": xhr: " + xhr + "   ajaxOptions: " + ajaxOptions + "    thrownError: " + thrownError);
            }
          });
        });

      }); //each station

    }, //success
    error: function (jqXHR, textStatus, errorThrown) {
      console.info("an error occured while trying to retrieve the stations, jqxhr=" + jqXHR + "    textstatus=" + textStatus + "    errorthrown=" + errorThrown);
    } //error
  });

} //end initMarkers


/**
 * Loads the actual data for the wind icons on the map
 */
function loadMapData() {
  //--the data for the hsl stations on the traject map
  var elem = $(elementSelecter).val();

  $.ajax({
    url: "mapDataServlet?type=mapdata",
    cache: false,
    dataType: "xml",
    success: function (data) {
      $(data).find('station').each(function (index) {
        var station = $(this);
        var stationId = $(station).attr("id");
        var value = $(station).attr(elem);
        var dir = $(station).attr("dd");
        var type = $(station).attr("type");
        var windcolor = $(station).attr("color" + currElem);
        var edgecolor = $(station).attr("color" + currElem + "Pre");

        if ($.isNumeric(value) && $.isNumeric(dir)) {
          markers[stationId].setIcon("windPicServlet?station=" + stationId + "&degrees=" + dir + "&value=" + value + "&type=" + type + "&windcolor=" + windcolor + "&edgecolor=" + edgecolor);
        } else {
          markers[stationId].setIcon(unknownIcon);
        }

      }); //each hslstation
    } //succsess
  });
} //end loadMapData


//-----zoom methods-------------------

/** for zooming */
function rezoom() {
  zoomType++;
  zoomIn(zoomType);
}

function zoomIn(type) {
  zoomType = type;
  if (zoomType > ZOOM_RADAR)
    zoomType = ZOOM_TRAJECT;

  if (zoomType === ZOOM_TRAJECT) {
    gmap.setZoom(9);
    gmap.setCenter(new google.maps.LatLng(51.93, 4.46));
  }
  else if (zoomType === ZOOM_BRIDGE) {
    gmap.setZoom(12);
    gmap.setCenter(new google.maps.LatLng(51.7, 4.62));
  }
  else if (zoomType === ZOOM_RADAR) {
    gmap.setZoom(8);
    gmap.setCenter(new google.maps.LatLng(51.6, 4.2));
  }
}



