  
/**
 * Base to set up a GoogleMap V3 frame
 * Supports Google Maps V3!
 * 
 * 
 * 
 * Is used by allRadars and radar.jsp
 * check any changes in both jsp's
 * 
 * @dependencies:
 * jquery
 * 
 * Copyright Meteo Consult (A MeteoGroup Company) 2011
 *
 *
 * Version	1.0
 *
 * History	1.0	  	09-06-2011		Rob Luiken			initial
 *
 */
var GoogleMap = function() {
	
	var _gmap;

	/*************************************
   *	initialize methods
   *************************************/
  /**
   * Initializes the data
   * @options: 
   * 	-	all GoogleMap options
   * plus:
   * 	-	panBounds: restrict panning
   * 
   * @return an instance of 
   */
  var _create = function(options){
    //--init Google Map
    //use any options as available in http://code.google.com/intl/nl-NL/apis/maps/documentation/javascript/reference.html#MapOptions 
    var gmapPresets = $.extend({
      //Google MapOptions
      zoom: 7, //required
      center: new google.maps.LatLng(52.0, 5.7), //required
      mapTypeId: google.maps.MapTypeId.ROADMAP //required
    }, options || {});
    var gmap = new google.maps.Map($("#map").get(0), gmapPresets);
    
    // Restrict panning
	var panBounds = options.panBounds || null;
    if (panBounds != null) {
      google.maps.event.addListener(gmap, "move", function(){
        var center = gmap.getCenter();
        if (panBounds.contains(center)) 
          return;
      });
    } //panBounds not null
  
	_gmap = gmap;
    return gmap;
  } //_initialize
 
  
  /**
   * convenience method to disable/enable default google map controls
   */
  var _doDefaultControls = function(){
  		_gmap.setOptions({streetViewControl:false});
		/* TODO: gaat niet goed boven de controls
		google.maps.event.addListener(_gmap, "mouseover", function(){
			console.info("in map");
			_gmap.setOptions({ zoomControl: true });
			_gmap.setOptions({ panControl: true });
			_gmap.setOptions({ mapTypeControl: true });
		});
		google.maps.event.addListener(_gmap, "mouseout", function(){
			console.info("uit map");
			_gmap.setOptions({ zoomControl: false });
			_gmap.setOptions({ panControl: false });
			_gmap.setOptions({ mapTypeControl: false });
		});
		*/
	} //_doDefaultControls
  
  var _finalize = function(){
    //GUnload();
  } //_finalize
  
  return {
    create: function(options){
      return _create(options);
    },
	doDefaultControls: function(){
      _doDefaultControls();
    },
	
    finalize: function(){
      _finalize();
    }
  };
  
} //end GoogleMap