/** 
 * wmsLayer - Put every WMS based map on top of Google Maps V3
 *
 * Copyright Meteo Consult (A MeteoGroup Company)
 * 
 * Version	1.0
 *
 * History	1.0.0		14-06-2010		Rob Luiken		Initial version based on MCWmsLayer of Arno Paanstra
 * 		1.0.1		24-05-2013		Rob Luiken		setEnabled added
 *
 *
 */ 
 
var WmsLayer = function(){
	
	var _layerId = null;
	var _gmap = null;
	var _tileLayer = null;
	
 	
	/**
	 * adds the layer to the googlemap
	 * @param {Object} gmap
	 * @param {Object} options
	 */
	var _add = function(gmap, options){
		//use any options as available in http://code.google.com/intl/nl-NL/apis/maps/documentation/javascript/reference.html#ImageMapTypeOptions
		var presets = $.extend({
			//Google ImageMapTypeOptions
			tileSize: new google.maps.Size(256, 256), //required
			opacity: 0.70, //required
			isPng: false, //required
			//custom options
			//mapPath: "http://mapserver.meteocon.nl/cgi-bin/mapserv?map=/var/www/Data/Data/mapserver/", //no-cache
			mapPath: "http://mapserver.meteocon.nl/gmaps-cache/?map=/var/www/Data/Data/mapserver/",
			mfile: "empty",	  //required
			layers: "empty",  //required
			tileName: null, //required, can be same as layers
			tileVersion: "1.0.0",
				
			wmsRequest: "GetMap",
			wmsVersion: "1.1.0",
			styles: "default",
			format: "image/gif",
			lSRS:"EPSG:4326",
			bgcolor: "0xFFFFF",
			transparent: true,
			tileWidth: 256,
			tileHeight: 256
		}, options || {});
		
		var tileW = presets.tileWidth;
		var tileH = presets.tileHeight;
		
		layerName = presets.mfile+"_"+presets.layers;
		
		if (presets.tileName==null) presets.tileName = presets.layers;
		
			
		var baseUrl = presets.mapPath+presets.mfile
			+ "&REQUEST=" + presets.wmsRequest
			+ "&SERVICE=WMS"
			+ "&VERSION=" + presets.wmsVersion
			+ "&LAYERS=" + presets.layers
			+ "&STYLES=" + presets.styles
			+ "&FORMAT=" + presets.format
			+ "&BGCOLOR="+ presets.bgcolor
			+ "&TRANSPARENT=" + presets.transparent
			+ "&SRS=" + presets.lSRS
			+ "&WIDTH=" + tileW
			+ "&HEIGHT=" + tileH
		
		var pngFormat = presets.format.toLowerCase().match(/png$/);
		
		//create tile layer
		var tileLayer = new google.maps.ImageMapType({
			getTileUrl: function(tile, zoom) {
		
				//get coordinates
				var zz = Math.pow(2,zoom);
				var pSW = new google.maps.Point( (tile.x * tileW) / zz ,((tile.y + 1) * tileH) /zz); 
				var pNE = new google.maps.Point( ((tile.x + 1) * tileW) / zz, (tile.y * tileH) /zz);
				
				var projection = gmap.getProjection();
				var lSW = projection.fromPointToLatLng(pSW);
				var lNE = projection.fromPointToLatLng(pNE);
				
				var bBbox = lSW.lng() + "," + lSW.lat() + "," + lNE.lng() + "," + lNE.lat();	
		
				var quadKey = _getQuadKey(tile.x, tile.y, zoom);
														
				var url = baseUrl
						+ "&BBOX=" + bBbox
						+ "&TILE=" + presets.tileName + "|" + presets.tileVersion + "|" + quadKey;				
				return url; 
			},
			tileSize: new google.maps.Size(tileW, tileH),
			opacity: presets.opacity,
			isPng: pngFormat,
			name: layerName
		});
		
		_tileLayer = tileLayer;
		
		_layerId = gmap.overlayMapTypes.length;
		
		//add to gmap
		gmap.overlayMapTypes.setAt(_layerId,_tileLayer); // set the overlay
		_gmap = gmap;
	}; //_addWmsLayer
	
	/**
	 * returns the quadrant key
	 * @param {Object} x
	 * @param {Object} y
	 * @param {Object} zoom
	 */
	var _getQuadKey =  function (x, y, zoom) {
		var quadKey = "";
		
	  	for (var i = zoom; i > 0; i--){
			mask = 1 << (i - 1);
			cell = 0;
			if ((x & mask) != 0)
				cell++;
			if ((y & mask) != 0)
				cell += 2;
			quadKey += cell;
		}
		return quadKey;
	}; //
	
	/**
	 * Enables/disables the layer 
	 */
	var _setEnabled = function(val) {
		var layer = null;
		if (val) 
			layer = _tileLayer;
		_gmap.overlayMapTypes.setAt(_layerId,layer); // set the overlay or null //TODO: only works well if this layer stays on a fixed index and no other layers are added intermediately
		//console.info("setLayer '"+_layerId+"  to "+layer);
	};
	
	var _finalize = function() {
		//clear all layers
		_gmap.overlayMapTypes.clear();
		_tileLayer = null;
	}; //_finalize
  
	return {
		add: function(gmap,options){
		  return _add(gmap,options);
		},
		setEnabled: function(val) {
			_setEnabled(val);
		},
		finalize: function(){
		  _finalize();
		}
	};
  
}; //end GoogleMap

