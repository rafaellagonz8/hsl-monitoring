/**
 * radaroverlay.js - Based on McRadar but now for Google Maps V3. Renamed to prevent conflicts
 *
 * Copyright Meteo Consult (A MeteoGroup Company) 2011
 *
 * Version  1.0.7  No longer at /fs01 because  we cannot guarantee backwards compatability anymore
 *
 * History  1.0      16-03-2011    Rob Luiken      Based on Radar.js V2.3 but ready for Google Maps V3
 *          1.0.2    13-03-2012    Rob Luiken      property cachedLoader added to be able to load radar images which are not available in cache
 *          1.0.3    14-03-2012    Rob Luiken      do not try to load image nor cache if no servlet is (yet) defined
 *          1.0.4    16-04-2012    Rob Luiken      image = null replaced by image = pixel.gif to prevent js error
 *          1.0.5    19-04-2012    Rob Luiken      imageFolder configurable and options in constructor
 *          1.0.6    18-06-2013    Rob Luiken      workaround for panning over dayline
 *          1.0.7    27-09-2013    Rob Luiken      do not redraw if the image is hidden
 */
function Radar(options) {

  var presets = $.extend({
    transparency: 35,
    imageFolder: "images/"
  }, options || {});

  this.transparency = presets.transparency;
  this.radarbounds = presets.radarbounds;
  this.imageFolder = presets.imageFolder;


  this.map_ = null;

  // Radar settings
  this.timestamp_ = null;
  this.dtg_ = null;
  this.servlet_ = null;
  this.interval_ = null;
  this.radarOff = false;

  this.rectan = new google.maps.Rectangle({
    strokeColor: '#FF0000',
    strokeOpacity: 0.8,
    strokeWeight: 2,
    fillColor: '#FF0000',
    fillOpacity: 0.35,
    map: this.map
  });

  this.stretchfactor_ = 1;


  /*
   this.zoom_			= null;
   this.border_		= 1;
   */
  //cache in a js Array of radar images
  this.caching_ = this._isCacheable();
  this.cached_ = null;

  //use the cached dataloader, if false the dataLoader is told to always load, even if not in cache
  this.cachedLoader_ = true;

  //this.blankOverlay = new RadarBlank();

  this.blankImg_ = document.createElement("img");
  this.blankImg_.src = this.imageFolder + "loading.gif";
  this.blankImg_.style.opacity = 0.8;
  this.blankImg_.style.position = "absolute";
  $(this.blankImg_).hide();

  //create new IMG element
  var thisRadar = this;
  this.img_ = document.createElement("img");
  this.img_.style.opacity = this.transparency;
  this.img_.style.position = "absolute";
  this.img_.src = this.imageFolder + "pixel.gif";


  $(this.img_).load(function () {
    thisRadar.imgOnLoad();
  });

  this.img_.onerror = function () {
    thisRadar.imgOnError();
  };

  this.img_.onabort = function () {
    thisRadar.imgOnAbort();
  };

}

Radar.prototype = new google.maps.OverlayView();

Radar.prototype.onAdd = function () {
  var pane = this.getPanes().overlayLayer;
  pane.appendChild(this.img_);
  pane.appendChild(this.blankImg_);
};

Radar.prototype.onRemove = function () {
  if (this.getPanes() != null) {
    var pane = this.getPanes().overlayLayer;
    pane.removeChild(this.img_);
    pane.removeChild(this.blankImg_);
  }
  //this.div_.parentNode.removeChild(this.img_);
  this.img_ = null;
  map_ = null;
  this.cached_ = null;
};

Radar.prototype.draw = function () {
  var self = this;
  setTimeout( self.redraw(true), 200);
};

Radar.prototype.setOff = function () {
  $(this.img_).hide();
  $(this.blankImg_).hide();
  this.radarOff = true;
};

Radar.prototype.setOn = function () {
  $(this.img_).show();
  this.radarOff = false;
};

/**
 * Sets the radar to a blank image
 */
Radar.prototype.setBlank = function () {

  //try to center
  var map_ = this.getMap(); // from super
  if (map_ != null && this.getProjection()) {
    var center = this.getProjection().fromLatLngToContainerPixel(map_.getCenter());
    this.blankImg_.style.left = Math.max(center.x - (this.blankImg_.width / 2), 0) + "px";
    this.blankImg_.style.top = Math.max(center.y + (this.blankImg_.height / 2), 0) + "px";
  }

  if (!this.radarOff) {
    $(this.blankImg_).show();
  }
};

/**
 * Sets the blankImg src
 * @param {Object} blankImg
 */
Radar.prototype.setBlankImage = function (src) {
  this.blankImg_.src = src;
};

/**
 * Can be called when there is no image loaded. This image will be stretched to the same size as the radar image and is mainly there to
 * replace the old image which may be outdated and may contain the wrong information.
 * Once a new image will be loaded this image will be replaced by the new image, therefore this image needs to be of the same size and opacity as the radar image.
 */
Radar.prototype.showErrorImg = function () {
  var errorImgSrc = this.imageFolder + "graypixel.gif";
  var currSrc = $(this.img_).attr("src");
  if (currSrc.toUpperCase() !== errorImgSrc.toUpperCase()) { //prevent bubble of events if this errorImg can not be found
    this.img_.src = errorImgSrc;
  }
};


/**
 * Sets the folder for the "blank" and "pixel" images
 * @param {Object} folder, default "images/"
 */
Radar.prototype.setImageFolder = function (folder) {
  this.imageFolder = folder;
};

Radar.prototype.setTransparency = function (transparency) {
  if (this.transparency !== transparency) {
    this.transparency = transparency;
    this.applyTransparency(this.transparency);
  }
};

Radar.prototype.applyTransparency = function (t) {
  if (this.img_ != null) {
    this.img_.style.opacity = 1 - (t / 100);
    this.img_.style.filter = "alpha(opacity=" + (100 - t) + ")";
  }
};

Radar.prototype.setServlet = function (servlet) {
  if (this.servlet_ !== servlet) {
    this.servlet_ = servlet;
    this._clearCache();
  }
};

Radar.prototype.getServlet = function () {
  return this.servlet_;
};

Radar.prototype.setDtg = function (dtg) {
  if (this.dtg_ == null || this.dtg_ !== dtg) {
    this.dtg_ = dtg;
    this.redraw(true);
  }
};

Radar.prototype.getDtg = function () {
  return this.dtg_;
};

Radar.prototype.setTimestamp = function (timestamp) {
  if (this.timestamp_ !== timestamp) {
    this.timestamp_ = timestamp;
    this._clearCache();
  }
};

Radar.prototype.getTimestamp = function () {
  return this.timestamp_;
};

Radar.prototype.setInterval = function (interval) {
  if (this.interval_ !== interval) {
    this.interval_ = interval;
    this._clearCache();
  }
};

Radar.prototype.getInterval = function () {
  return this.interval_;
};

Radar.prototype.setRadarBounds = function (radarbounds) {
  if (this.radarbounds !== radarbounds) {
    this.radarbounds = radarbounds;
  }
};

Radar.prototype.getRadarBounds = function () {
  return this.radarbounds;
};

Radar.prototype.setCachedLoader = function (val) {
  this.cachedLoader_ = false;
};

Radar.prototype.redraw = function (force) {

  var map_ = this.getMap(); // from super
  // Overlay isn't shown if map is null.... exit!
  // Dtg null...... exit!
  if (map_ == null || this.dtg_ == null || map_.getBounds() == null) {
    this.setBlank();
    return;
  }

  if (this.radarOff) {
    return;
  }

  // Panning fires a not forced redraw.
  // If new bounds still within previous bounds (including margin) ..... exit!
  var mapBounds = map_.getBounds();

  if (this.zoom_ == null || this.zoom_ !== map_.getZoom()) {
    force = true;
    this.zoom_ = map_.getZoom();
  }

  if (!force) {
    if (this._containsBounds(mapBounds))
      return;
    else {
      this.cached_ = new Array();
    }
  }
  else {
    this.cached_ = new Array();
  } //else (=force)

  var mapSW = mapBounds.getSouthWest();
  var mapNE = mapBounds.getNorthEast();

  var north = mapNE.lat();
  var south = mapSW.lat();
  var west = mapSW.lng();
  var east = mapNE.lng();


  // New image bounds
  var imgSW = null;
  var imgNE = null;

  // if across the day line
  if (east < west && this.radarbounds) {
    //just use the complete radarbounds without optimization. With the current radars we can only pass the day line on a large zoomlevel and we could use the complete radar image without performance loss
    //TODO: make this smarter
    imgSW = this.radarbounds.getSouthWest();
    imgNE = this.radarbounds.getNorthEast();
  }
  else { //do smart calculations to get the image bounds

    // round to tile bounds to be able to cache the radar requests and
    // reduce requested area based on Radar bounds
    var block = 256;
    var zoomPow = Math.pow(2, this.zoom_);
    var mapSize = block << this.zoom_;
    // southwest to pixel then to tile px and back to Mercator for tile edge
    var rbSW = (this.radarbounds == null) ? null : this.radarbounds.getSouthWest();

    if ((rbSW != null) && (south < rbSW.lat())) { // reduce on radar
      // bounds
      south = rbSW.lat();
    }
    else { // or adjust to tile edge
      var sinLatS = Math.sin(south * Math.PI / 180);
      var pixelS = (0.5 - Math.log((1 + sinLatS) / (1 - sinLatS)) / (4 * Math.PI)) * block * zoomPow;
      var pixTileS = block * Math.ceil(pixelS / block); // South bottom
      var yS = 0.5 - (this._clip(pixTileS, 0, mapSize - 1) / mapSize);
      south = 90 - 360 * Math.atan(Math.exp(-yS * 2 * Math.PI)) / Math.PI;
    }
    if ((rbSW != null) && (west < rbSW.lng())) { // reduce on radar bounds
      west = rbSW.lng();
    }
    else { // or adjust to tile edge
      var pixelW = ((west + 180) / 360) * block * zoomPow;
      var pixTileW = block * Math.floor(pixelW / block); // utter West
      var xW = (this._clip(pixTileW, 0, mapSize - 1) / mapSize) - 0.5;
      west = 360 * xW;
    }

    // northeast to pixel then to tile px and back to Mercator for tile edge
    var rbNE = (this.radarbounds == null) ? null : this.radarbounds.getNorthEast();
    if ((rbNE != null) && (north > rbNE.lat())) { // reduce on radar bounds
      north = rbNE.lat();
    }
    else { // or adjust to tile edge
      var sinLatN = Math.sin(north * Math.PI / 180);
      var pixelN = (0.5 - Math.log((1 + sinLatN) / (1 - sinLatN)) / (4 * Math.PI)) * block * zoomPow;
      var pixTileN = block * Math.floor(pixelN / block); // North top
      var yN = 0.5 - (this._clip(pixTileN, 0, mapSize - 1) / mapSize);
      north = 90 - 360 * Math.atan(Math.exp(-yN * 2 * Math.PI)) / Math.PI;
    }
    if ((rbNE != null) && (east > rbNE.lng())) { // reduce on radar bounds
      east = rbNE.lng();
    }
    else { // or adjust to tile edge
      var pixelE = ((east + 180) / 360) * block * zoomPow;
      var pixTileE = block * Math.ceil(pixelE / block); // utter East
      var xE = (this._clip(pixTileE, 0, mapSize - 1) / mapSize) - 0.5;
      east = 360 * xE;
    }

    // calculated image bounds
    imgSW = new google.maps.LatLng(south, west);
    imgNE = new google.maps.LatLng(north, east);

  } //else (=do smart calculations to get the image bounds)

  // Set requested bounds including border
  this.bounds_ = new google.maps.LatLngBounds(imgSW, imgNE);

  // Deterime pixel positions
  var projection = this.getProjection(); //Overlay projection

  var c1 = projection.fromLatLngToDivPixel(imgSW);
  var c2 = projection.fromLatLngToDivPixel(imgNE);

  var w = Math.abs(c2.x - c1.x);
  var h = Math.abs(c2.y - c1.y);
  var l = Math.min(c2.x, c1.x);
  var t = Math.min(c2.y, c1.y);

  // Hide current image
  $(this.blankImg_).hide();

  // Set new image settings
  this.img_.style.left = l + "px";
  this.img_.style.top = t + "px";
  this.img_.className = "radarImage";

  w = Math.round(w / this.stretchfactor_);
  h = Math.round(h / this.stretchfactor_);

  if (this.caching_ && typeof this.cached_[this.dtg_] === 'object') {
    this.img_.src = this.cached_[this.dtg_].src;
  }
  else {
    if (this.servlet_ == null) {
      //do not try to load image nor cache if no servlet defined
      this.img_.src = this.imageFolder + "pixel.gif";
    }
    else {
      this.img_.src = this.servlet_
        + "?type=radar"
        + "&dtg=" + this.dtg_
        + "&timestamp=" + this.timestamp_
        + "&latmin=" + imgSW.lat()
        + "&latmax=" + imgNE.lat()
        + "&lonmin=" + imgSW.lng()
        + "&lonmax=" + imgNE.lng()
        + "&width=" + w
        + "&height=" + h
        + (this.interval_ ? "&interval=" + this.interval_ : "")
        + ((this.cachedLoader_ === false) ? "&cache=false" : "");


      this.applyTransparency(this.transparency);

      this.img_.style.width = w + "px";
      this.img_.style.height = h + "px";

      if (this.caching_) {
        this.cached_[this.dtg_] = new Image();
        this.cached_[this.dtg_].src = this.img_.src;
      }
    }
  } //else
};


Radar.prototype._containsBounds = function (bounds) {
  if (this.bounds_ == null)
    return null;
  else {
    return ( this.bounds_.contains(bounds.getNorthEast()) && this.bounds_.contains(bounds.getSouthWest()));
  }
};

Radar.prototype._clearCache = function () {
  this.cached_ = null;
  this.cached_ = new Array();
};

Radar.prototype._clip = function (n, minValue, maxValue) {
  return Math.min(Math.max(n, minValue), maxValue);
};

Radar.prototype._isCacheable = function () {
  var agent = navigator.userAgent.toLowerCase();
  var major = parseInt(navigator.appVersion);
  var minor = parseFloat(navigator.appVersion);

  if (agent.indexOf("msie") !== -1) {
    if (agent.indexOf("msie 6.") >= 0) {
      return false;
    }
  }
  return true;
};

Radar.prototype.imgOnLoad = function () {
  google.maps.event.trigger(this, "imgonload");
};

Radar.prototype.imgOnError = function () {
  google.maps.event.trigger(this, "imgonerror");
};

Radar.prototype.imgOnAbort = function () {
  google.maps.event.trigger(this, "imgonabort");
};
