<%@ page import="meteoconsult.products.hsl.App"%>
<%@ page import="meteoconsult.util.StringUtil"%>

<jsp:useBean id="dataPageBean" scope="session" class="meteoconsult.products.hsl.DataPageBean" />

<%--start uit datapage.jsp--%>
<%long now = new java.util.Date().getTime();%>

<table id='overall'>

	<tr>
		<td>
			<table id='bridge_charts'>
				<tr>
					<td id="selected_station"><%=dataPageBean.getBridgeStationName()%></td>
				</tr>
				<tr>
					<td><img id='bridge_fcst_chart' src='stationGraphServlet?type=fcst&station_id=<%=App.ID_HSL_BRIDGE%>&dummy=<%=now%>'></td>
				</tr>
				<tr>
					<td><img id='bridge_obs_chart' src='stationGraphServlet?type=obs&station_id=<%=App.ID_HSL_BRIDGE%>&dummy=<%=now%>'></td>
				</tr>
			</table> <%-- bridge_charts --%>
		</td>

		<td id='space_max_wind'>
			<table>
				<tr>
					<td><div id='max_wind'><%=dataPageBean.getActualWindBridgeTable()%></div>
					</td>
				</tr>
				<tr>
					<td>max Brug Holl. Diep</td>
				</tr>
				<tr>
					<td><div id='max_wind'><%=dataPageBean.getMaxWindBridgeTable()%></div>
					</td>
				</tr>
				<tr>
					<td><hr>
					</td>
				</tr>
				<tr>
					<td>max 1 min. stations</td>
				</tr>
				<tr>
					<td><div id='max_wind'><%=dataPageBean.getMaxWind1MinTable()%></div>
					</td>
				</tr>

				<tr>
					<td>max 10 min. stations</td>
				</tr>
				<tr>
					<td><div id='max_wind'><%=dataPageBean.getMaxWind10MinTable()%></div>
					</td>
				</tr>

			</table>
		</td>
		<%-- space_nmax_wind --%>
	</tr>

	<tr>
		<td colspan="2">
			<table id="individual">
				<tr>
					<td id="selected_station" colspan="2"><%=dataPageBean.getSelectedStationName()%></td>
					<%--<td><div id="space_beep"><%=dataPageBean.getBeep()%></div></td> --%>
					<td>
						<div id="space_beep"></div><%=dataPageBean.getAlarmMesssage()%>
					</td>
				</tr>
				<tr>
					<td colspan="2"><img id='var_obs_chart' src='stationGraphServlet?type=obs&dummy=<%=now%>'></td>
				</tr>

				<tr>
					<td>
						<div id='space_minute_table'>
							<%=dataPageBean.getWindTable()%>
						</div>
					</td>

					<td>
						<div id='space_tabs'>
							<%-- <%= dataPageBean.getAlarmTables() %> --%>

							<div id="tabs" class="ui-tabs ui-widget ui-widget-content ui-corner-all">
								<ul class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all">
									<li class="ui-state-default ui-corner-top ui-tabs-selected ui-state-active"><a href="#tab-1">Summary</a>
									</li>
									<li class="ui-state-default ui-corner-top"><a href="#tab-2">North</a>
									</li>
									<li class="ui-state-default ui-corner-top"><a href="#tab-3">South</a>
									</li>
								</ul>
								<div id="tab-1" class="ui-tabs-panel ui-widget-content ui-corner-bottom">
									<p class="warning_title">WWS:</p>
									<%=dataPageBean.getLatestAlarmFase()%><br />
									<p class="warning_title">BAPPS:</p>
									<%=dataPageBean.getBappsWarnings()%><br />
									<p class="warning_title">HSL alarm:</p>
									<%=dataPageBean.getAlarmStatusSummary()%><br />
								</div>
								<div id="tab-2" class="ui-tabs-panel ui-widget-content ui-corner-bottom">
									<div class='space_alarm_table'><%=dataPageBean.getNorthAlarms()%></div>
									<!-- space_alarm  -->
								</div>
								<div id="tab-3" class="ui-tabs-panel ui-widget-content ui-corner-bottom">
									<div class='space_alarm_table'><%=dataPageBean.getSouthAlarms()%></div>
									<!-- space_alarm  -->
								</div>
							</div>
							<!-- tabs  -->

						</div> <!-- space_tabs  -->
					</td>

					<td id="legend"><%=dataPageBean.getLegendTable()%></td>

				</tr>

			</table> <%-- individual --%>
		</td>
		<%-- top --%>

	</tr>
</table>
<%-- overall --%>

<script>
	/**
	* Is loaded, each time this page is refreshed
	*/
	showAlarmSound(<%=StringUtil.dbItQuoted( dataPageBean.getAlarmSound() )%>);
	
	var initTabs = function() {
		$( "#tabs" ).tabs();

		//resize tabs to designated space
		$('#tabs div.ui-tabs-panel').height(function() {
			return $('#space_tabs').height()
				   - $('#space_tabs #tabs ul.ui-tabs-nav').outerHeight(true)
				   - ($('#tabs').outerHeight(true) - $('#tabs').height())
                   // visible is important here, sine height of an invisible panel is 0
				   - ($('#tabs div.ui-tabs-panel:visible').outerHeight(true)  
				   - $('#tabs div.ui-tabs-panel:visible').height()+2);
		});
	
	}();
	
</script>