<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html>

<%@ page import="meteoconsult.products.hsl.servlet.StationGraphServlet"%>
<%@ page import="meteoconsult.database.tables.Table"%>
<%@ page import="java.util.Map"%>
<%@ page import="ChartDirector.XYChart"%>


<%--
We found issues with the caching of xycharts in a multithreaded environment. This page shows us the current cache size

TODO: for testing purposes only, remove this page after a whil

Rob Luiken 24 July 2014
 --%>

<%
StringBuffer sbSyncMapKeys = new StringBuffer();
int sz;	
Map<Table,XYChart> syncMap = StationGraphServlet.getCacheForTest();		
synchronized (syncMap) {  
	sz = syncMap.size();
	for (Table key: StationGraphServlet.getCacheForTest().keySet()) {
		sbSyncMapKeys.append(key.hashCode()).append(" ");
	}
}
%>


<html>
<head>
        <meta http-equiv="refresh" content=3 />
</head>

<body>

Test for caching in multithreaded environment <%=new java.util.Date()%><br />
<br/>
size = <%=sz%> <br/>
keys = <%=sbSyncMapKeys%> <br/>

</body>
</html>