<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html>

<!--
- Author: luiken
- Date: 23-okt-2007
- Copyright: (c) MeteoGroup
- Description: 
-
-->

<%-- JSP page directives (<%@ page... %>) --%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.TimeZone"%>

<%@ page import="meteoconsult.products.hsl.App"%>
<%@ page import="meteoconsult.util.DateTime"%>
<%@ page import="meteoconsult.products.hsl.HslDataProvider"%>

<%-- <%@ include file="WEB-INF/loginCheck.jspf" %> --%>

<jsp:useBean id="dataPageBean" scope="session" class="meteoconsult.products.hsl.DataPageBean" />

<%
	//init
	TimeZone tz = DateTime.TIMEZONE_NL;
	session.setAttribute("timeZone",tz);
	HslDataProvider dataProvider = new HslDataProvider();
	session.setAttribute("dataProvider",dataProvider);
	dataProvider.setTimeZone(tz);
	//dataProvider.refresh(new Date(), obsEndDate);
	
	dataPageBean.setTimeZone(tz);
	dataPageBean.setHslDataProvider(dataProvider);

	SimpleDateFormat sdfDisplay = new SimpleDateFormat(meteoconsult.util.DateTime.DB_YEAR_TO_SEC);
	sdfDisplay.setTimeZone(tz);
%>

<%--HTML starts here--%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />

<title>HSL - MeteoGroup</title>

<link rel="stylesheet" type="text/css" href="css/smoothness/jquery-ui-1.8.5.custom.css" />
<link rel="StyleSheet" type="text/css" href="css/meteocon.css" />

<%-- script url voor GoogleMap --%>
<%-- <script src="http://maps.google.com/maps?oe=utf-8&file=api&v=2.x&key=<%= googleKey %>" type="text/javascript"></script> --%>

<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true&client=gme-meteo"></script>

<script type="text/javascript" src="js/gmap/meteoconsult.gmap3.googlemap-1.0.0.js"></script>
<script type="text/javascript" src="js/gmap/meteoconsult.gmap3.radaroverlay-1.0.7.js"></script>
<script type="text/javascript" src="js/gmap/meteoconsult.gmap3.wmslayer-1.0.1.js"></script>



<script src="js/lib/fauxconsole-1.0.1.js"></script>
<script src="js/jquery-1.9.1.min.js"></script>

<script src="js/lib/jquery.ui-1.8.2.min.js"></script>

<script src="js/gmap/hslGoogleMap.js"></script>

<%-- This script delivers the initial parameters to the JavaScript in timerCore.js--%>
<script type="text/javascript">
	
	var googleMap = new GoogleMap();
	var wmsLayer = new WmsLayer();
	
	var interval = 30;
	var currElem = "fx";
	
	$(document).ready(function() {
		
		var googleMapOptions = {
			zoom: 9,
			minZoom: 5,
			maxZoom: 14,
			streetViewControl:false,
			mapTypeId: google.maps.MapTypeId.SATELLITE,
            gestureHandling: 'greedy'
		};	
		var gmap = googleMap.create(googleMapOptions);

		initHslMonitor(gmap);
		
		//create radar overlay
		radar = new Radar();
		radar.setTimestamp(null);
		radar.setDtg( $("#dtg").val() );
		radar.setServlet('nlRadarTypeServlet');
		radar.setTransparency(60);
		radar.setMap(gmap);

		//rail overlay
		var kmlOptions = {	
            clickable: false,
            map: gmap,
            url: "http://external.meteo.nl/hsl/hslTraject.kml"
		};
		new google.maps.KmlLayer(kmlOptions);
		
		var wmsOverlayOptions = {	
            mfile: "nsrail.map",
            layers: "nsrail"
        };
		wmsLayer.add(gmap, wmsOverlayOptions);
		
		//resize map
		var w = $("#map_page").width();
		var h = $(window).height() -40;
		$("#map").width(w);
		$("#map").height(h);
		
	});
	
	$(window).unload(function() {
		wmsLayer.finalize();
		googleMap.finalize();
	});
	
</script>
</head>

<body>

	<table id='base_table'>
		<!-- row 1 - header -->
		<tr id="header_row" style='vertical-align: top'>
			<td colspan='2'>
				<table id='header_table'>
					<tr>
						<td><img src="images/logo_meteogroup_small.png">
						</td>
						<td>
							<select id="dtgSelecter">
									<option>--please wait--</option>
							</select> 
							<select id="intervalSelecter" onchange="refreshAll();">
									<option value=10>10 min</option>
									<%-- synchronize SELECTED with initInterval --%>
									<option value=20>20 min</option>
									<option value=30 SELECTED>30 min</option>
									<option value=60>60 min</option>
									<option value=120>2 uur</option>
									<option value=720>12 uur</option>
									<option value=1440>24 uur</option>
							</select>
						</td>
						<td id='app_name' width='50%'>- HSL monitor -</td>
						<td>
							<select name="elementSelecter" id="elementSelecter" onchange="loadMapData();">
									<option value="ff">wind speed</option>
									<%-- synchronize SELECTED with initial element --%>
									<option value="fx" SELECTED>wind gusts</option>
							</select>
							<button onclick='rezoom();'>zoom</button>
						</td>
						<td id='reset'>
						  <!-- <button onclick='playSound("wav/ding");'><img src="images/note.gif">test sound</button> -->
							<button onclick='logout();'>reset</button> 
							<a href="file:///churada.nl.meteogroup.net/OD/Instructies, handl, formulieren/Instructies Werk/HSL" target="_top"> <img src='images/info.gif'></a>
						</td>
					</tr>
				</table></td>
		</tr>
		<!-- row 2 - body -->
		<tr style='vertical-align: top'>
			<!-- column 1 - hsl graphs and tables -->
			<td id='data_page'>
				<div id='dataBlock'>- wait for data -</div>
			</td>

			<!-- column 2 - hsl traject map and hsl bridge map -->
			<td id='map_page'>
				<div id='map'>- please wait for map -</div>
			</td>
		</tr>
	</table>
	
</body>