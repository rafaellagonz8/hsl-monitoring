package meteoconsult.products.hsl;

import java.util.HashSet;
import java.util.logging.Logger;

import javax.servlet.ServletContextEvent;

import meteoconsult.database.connector.JDBCConnection;
import meteoconsult.database.tables.rbsadmin.HslWarningState;
import meteoconsult.lib.common.radar.RadarProfile;
import meteoconsult.products.hsl.limit.AlarmLimitValue;
import meteoconsult.products.hsl.limit.LimitProvider;

/**
 * If this Listener is defined in thw web.xml file like:
 * <block>
 *
 *   <listener>
 *     <listener-class>meteoconsult.products.mcpayment.App</listener-class>
 *  </listener>
 *
 * </block>
 * the contextInitialized method will be called at startup of the servlet and contextDestroyed at end time.
 *
 * @author not attributable
 * @version 1.0
 */
public class App implements javax.servlet.ServletContextListener {
	public final static Logger log = Logger.getLogger("meteoconsult.products.hsl");

	public final static int ID_HSL_BRIDGE = 1606362;
	public final static int ID_HSL_GSM_MAST	= 1606361;
	public final static int ID_HSL_HELLEGATSPLEIN	= 1606369;
	public final static int ID_HSL_MAASDAM = 1606364;
	public final static int ID_HSL_MARK	= 1606367;
	public final static int ID_HSL_MOERDIJKBRUG	= 1606363;
	public final static int ID_HSL_HOLL_SLUIS	= 1606365;
	public final static int ID_HSL_KLUNDERT	= 1606366;

	public final static int ID_VOORSCHOTEN = 6215; 
	public final static int ID_AMS = 6240;
	public final static int ID_RTM = 6344;
	public final static int ID_GLZ = 6350;

	public final static int ID_IJMUIDEN  				=	6225;
	public final static int ID_ZANDVOORT 				= 1606216;
	public final static int ID_VLISSINGEN 			= 6310;
	public final static int ID_OOSTERSCHELDE 		= 6312;
	public final static int ID_VLAKTE_VAN_RAAN 	= 6313;
	public final static int ID_HANSWEERT 				= 6315;
	//public final static int ID_SCHAAR_WP 				= 6316;
	public final static int ID_WESTDORPE_AWS 		= 6319;
	public final static int ID_LICHTEILAND_GOEREE = 6320;
	public final static int ID_EUROPLATFORM 		= 6321;

	public final static int ID_OUDDORP 					= 1606326;
	public final static int ID_HOEK_VAN_HOLLAND = 6330;
	public final static int ID_MAROLLEGAT 			= 6331;
	public final static int ID_TER_HEIJDE 			= 1606336;
	public final static int ID_WOENSDRECHT 			= 6340;
	public final static int ID_ROTTERDAM_GEULHAVEN = 6343;
	//new 1 jul 2014
	public final static int ID_ASSENDELFT = 99901;
	public final static int ID_MUIDEN = 99902;
	public final static int ID_NIEUW_VENNEP = 99903;
	public final static int ID_IJMOND = 6209;
	public final static int ID_STAVENISSE = 6324;
	public final static int ID_CADZAND = 6308;
	public final static int ID_HOOFDPLAAT = 6311;

	//new 29 sept 2018
	public final static int ID_WILHELMINADORP = 6323;

	
	public final static String TYPE_1_min = "1min";
	public final static String TYPE_10_min = "10min";
	
	

	public final static int[] NORTH_STATS = { ID_AMS,ID_VOORSCHOTEN,ID_ROTTERDAM_GEULHAVEN
		,ID_RTM,ID_IJMUIDEN,ID_ZANDVOORT
		,ID_HOEK_VAN_HOLLAND,ID_TER_HEIJDE,ID_LICHTEILAND_GOEREE,ID_EUROPLATFORM
		//new
		,ID_IJMOND,ID_MUIDEN
	};

	public final static int[] SOUTH_STATS = { ID_RTM ,ID_ROTTERDAM_GEULHAVEN,ID_HSL_MAASDAM
		,ID_HSL_MARK,ID_GLZ, ID_VLISSINGEN, ID_OOSTERSCHELDE, ID_VLAKTE_VAN_RAAN, ID_HANSWEERT       
		, ID_WESTDORPE_AWS, ID_MAROLLEGAT, ID_WOENSDRECHT    
		,ID_HOEK_VAN_HOLLAND,ID_TER_HEIJDE,ID_LICHTEILAND_GOEREE,ID_EUROPLATFORM
		//new
		,ID_ASSENDELFT,ID_NIEUW_VENNEP,ID_STAVENISSE,ID_CADZAND,ID_HOOFDPLAAT
		//new 27.09.2018
		,ID_WILHELMINADORP
	};

	/**
	 * contains bridge and wws stations
	 */
	public final static int[] BRIDGE_STATS = { ID_HSL_BRIDGE,ID_HSL_GSM_MAST,ID_HSL_HELLEGATSPLEIN
		,ID_HSL_MOERDIJKBRUG,ID_HSL_HOLL_SLUIS,ID_HSL_KLUNDERT ,ID_HSL_MAASDAM ,ID_HSL_MARK
	};

	public static HashSet<Integer> NORTH_SET = new HashSet<Integer>();
	public static HashSet<Integer> SOUTH_SET = new HashSet<Integer>();
	public static HashSet<Integer> BRIDGE_SET = new HashSet<Integer>(); 
	public static HashSet<Integer> SOUTHPLUSBRIDGE_SET = new HashSet<Integer>();
	public static HashSet<Integer> ALTSTATIONS_SET = new HashSet<Integer>();

	public final static int FLAG_NORTH = 1;
	public final static int FLAG_SOUTH = 2;
	public final static int FLAG_BRIDGE = 4;
	public final static int FLAG_WWS = 8;
	public final static int FLAG_WORST = 16;

	public final static int UNKNOWN_COLORCODE  = 0xa8a8a8;
	public final static int DEGRADED_MODE  = 990;
	

	public final static String CSS_DD   = "w_dd";
	public final static String CSS_UNKNOWN = "unknown";


	//alarm icons
	public final static String ICON_OPEN = "images/state/red.png";
	public final static String ICON_OPEN_SELECTED = "images/state/redSelected.png";

	public final static String ICON_REX = "images/state/blue.png";
	public final static String ICON_REX_SELECTED = "images/state/blueSelected.png";
	public final static String ICON_REX_OPEN = "images/state/redblue.png";

	public final static String ICON_SURPRESSED = "images/state/yellow.png";
	public final static String ICON_SURPRESSED_SELECTED = "images/state/yellowSelected.png";
	public final static String ICON_SURPRESSED_OPEN = "images/state/redyellow.png";

	public final static String ICON_HANDLED = "images/state/green.png";
	public final static String ICON_HANDLED_SELECTED = "images/state/greenSelected.png";
	public final static String ICON_HANDLED_OPEN = "images/state/redgreen.png";

	public final static String ICON_BLANK = "images/state/white.png";
	public final static String ICON_BLANK_OPEN = "images/state/whiteOpen.png";
	public final static String ICON_BLANK_CLOSED = "images/state/whiteClosed.png";	
	
	private static RadarProfile radarProfile;

	
	//private static HashMap<Integer, LimitStorable> stationLimits = new HashMap<Integer, LimitStorable>()
	private static LimitProvider limitProvider = new LimitProvider();

	/**
	 * Creates an instance of the AppServletContextListener and puts the application-scope values in the constantsMap.
	 */
	public App() {
	}

	/**
	 * contextInitialized
	 *
	 * @param sce ServletContextEvent
	 */
	public void contextInitialized(ServletContextEvent sce) {
		log.info("-- HSL application starts --");
		init();
	}



	/**
	 * Initializes the application variables. Is only called once.
	 */
	private void init() {
		radarProfile = RadarProfile.NL.clone();
		radarProfile.setRadarServlet("nlRadarServlet");
		radarProfile.setRadarTypeServlet("nlRadarTypeServlet");
		
		for (int stat: NORTH_STATS) {
			NORTH_SET.add(stat);
		}
		for (int stat: SOUTH_STATS) {
			SOUTH_SET.add(stat);
		}
		for (int stat: BRIDGE_STATS) {
			BRIDGE_SET.add(stat);
		}

		SOUTHPLUSBRIDGE_SET.addAll(SOUTH_SET);
		SOUTHPLUSBRIDGE_SET.addAll(BRIDGE_SET);
		
		ALTSTATIONS_SET.addAll(NORTH_SET);
		ALTSTATIONS_SET.addAll(SOUTH_SET);
		
		//load the limits per station
		limitProvider.init();
	}

	/**
	 * contextDestroyed
	 *
	 * @param sce ServletContextEvent
	 */
	public void contextDestroyed(ServletContextEvent sce) {
		JDBCConnection.destroy();
		log.info("-- bye bye, HSL application exits --");
	}

	public static RadarProfile getRadarProfile() {
		return radarProfile;
	}
	
	/**
	 * Alleen nodig voor BRIDGE
	 * @param dd wind in knots
	 * @return
	 */
	public static String ddCssClass(Object dd) {
		if (( dd!=null) && (dd instanceof Number)) {
			int idd = ((Number)dd).intValue();
			if ( (idd>=225) && (idd<=275) ) 
				return CSS_DD;
			else
				return null;
		}
		else
			return CSS_UNKNOWN;

	}



	/**
	 * De alarmen met de bijbehorende kleur
	 */
	public enum AlarmLimit {
		ALARM_STOP ("alarm STOP", 0, 0xBF00FF, HslWarningState.FASE_000),
		PRE_ALARM_STOP ("pre alarm STOP", 0, 0xD358F7, HslWarningState.FASE_PRE_000),
		ALARM_80 ("alarm 80", 80, 0xFE2E2E, HslWarningState.FASE_080), 
		PRE_ALARM_80 ("pre alarm 80", 80, 0xFA5858, HslWarningState.FASE_PRE_080),
		ALARM_160 ("alarm 160", 160, 0xFE9A2E, HslWarningState.FASE_160),
		PRE_ALARM_160 ("pre alarm 160", 160, 0xF7BE81, HslWarningState.FASE_PRE_160),
		ALARM_220 ("alarm 220", 220, 0xF7FE2E, HslWarningState.FASE_220), 
	  PRE_ALARM_220 ("pre alarm 220", 220, 0xF3F781, HslWarningState.FASE_PRE_220),
		NORMAL ("no alarm", 300, 0xFFFFFF, HslWarningState.FASE_300);
		
		

		private final String descr;  
		private final int kmh;
		private final int colorCode;
		private final Integer warningStateCode;

		AlarmLimit(String descr, int kmh, int colorCode, Integer warningStateCode) {
			this.descr = descr;
			this.kmh = kmh;
			this.colorCode = colorCode;
			this.warningStateCode= warningStateCode; 
		}
		public String descr() { return descr; }
		public int kmh() { return kmh; }
		public int colorCode() { return colorCode; }
		public Integer warningStateCode() { return warningStateCode; }
		
	} //enum AlarmLimit

	/**
	 * Alle alarmlimieten geordend van ernstigst tot minst ernstig
	 */
	public static AlarmLimit[] alarmLimits = new AlarmLimit[] { 
		AlarmLimit.ALARM_STOP, 
		AlarmLimit.PRE_ALARM_STOP, 
		AlarmLimit.ALARM_80, 
		AlarmLimit.PRE_ALARM_80, 
		AlarmLimit.ALARM_160, 
		AlarmLimit.PRE_ALARM_160, 
		AlarmLimit.ALARM_220, 
		AlarmLimit.PRE_ALARM_220,
		AlarmLimit.NORMAL
	};

	/**
	 * Uses the HslWarningState.warningStae to get the corresponding AlarmLimit
	 * @param warningStateCode
	 * @return
	 */
	public static AlarmLimit getAlarmLimitFromHslWarningState(Integer warningStateCode) {
		if (warningStateCode==null) 
			return null;
		for (AlarmLimit alarmLimit: alarmLimits) {
			if (warningStateCode.equals(alarmLimit.warningStateCode)) {
				return alarmLimit;
			}
		} //for 
		return null;
	}
	

	/**
	 * 
	 * @return
	 */
	public static LimitProvider getLimitProvider() {
		return limitProvider;
	}
	
	public static AlarmLimitValue getWorstLimit(int stationId, Number f, Number dd) {
		return getLimitProvider().getWorstLimit(stationId, f, dd);
	}
	
} //end class
