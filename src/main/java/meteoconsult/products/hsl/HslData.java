package meteoconsult.products.hsl;

import java.util.Date;

import meteoconsult.products.hsl.limit.AlarmLimitValue;
import flexjson.JSON;


public class HslData implements Cloneable  {
	
	public final static int ONE_MIN_STATION = 1;
	public final static int TEN_MIN_STATION = 10;
	
	public final static float PRE_ALARM_FACTOR = 1.1f;
	
	private int statId;
	private String orgName;
	private String name;
	private Date dtg;
	private Integer ff;
	private Integer dd;
	private Integer fx;
	private Double lat;
	private Double lon;
	
	private int type = TEN_MIN_STATION;

	public HslData(int statId) {
		this.statId = statId;
	}
	

	/**
	 * @return the statId
	 */
	public int getStatId() {
		return statId;
	}
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * @return the orgName
	 */
	public String getOrgName() {
		return orgName;
	}
	
	/**
	 * @param orgName the orgName to set
	 */
	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}
	
	public Double getLat() {
		return lat;
	}

	public void setLat(Double lat) {
		this.lat = lat;
	}
	
	public Double getLon() {
		return lon;
	}
	
	public void setLon(Double lon) {
		this.lon = lon;
	}
	
	/**
	 * @return the dtg
	 */
	public Date getDtg() {
		return dtg;
	}
	
	/**
	 * @param dtg the dtg to set
	 */
	public void setDtg(Date dtg) {
		this.dtg = dtg;
	}
	
	public Integer getFf() {
		return ff;
	}
	
	public void setFf(Integer ff) {
		this.ff = ff;
	}
	
	public Integer getDd() {
		return dd;
	}

	public void setDd(Integer dd) {
		this.dd = dd;
	}

	public Integer getFx() {
		return fx; 
	}

	public void setFx(Integer fx) {
		this.fx = fx;
	}
	
	public int getType() {
		return type;
	}

	
	/**
	 * Either HslData.ONE_MIN_STATION or HslData.TEN_MIN_STATION
	 * @param type
	 */
	public void setType(int type) {
		this.type = type;
	}
	
	public AlarmLimitValue getAlarmLimitFf() {
		return App.getWorstLimit(statId, ff, dd);
	}
	
	/**
	 * convenience method to get access to the AlarmLimit.colorCode
	 * @return
	 */
	public int getFfColorCode() {
		return getColorCode(getAlarmLimitFf());
	}
	
	@JSON(include=false)
	public AlarmLimitValue getPreAlarmLimitFf() {
		Float ffPre = (ff==null) ? null : ff * PRE_ALARM_FACTOR;
		return App.getWorstLimit(statId, ffPre, dd);
	}
	
	public AlarmLimitValue getAlarmLimitFx() {
		return App.getWorstLimit(statId, fx, dd);
	}
	
	/**
	 * convenience method to get access to the AlarmLimit.colorCode
	 * @return
	 */
	public int getFxColorCode() {
		return getColorCode(getAlarmLimitFx());
	}
	
	@JSON(include=false)
	public AlarmLimitValue getPreAlarmLimitFx() {
		Float fxPre = (fx==null) ? null : fx * PRE_ALARM_FACTOR;
		return App.getWorstLimit(statId, fxPre, dd);
	}
	
	
	
	/**
	 * convenience method to get access to the AlarmLimit.colorCode
	 * 
	 * @return
	 */
	@JSON(include=false)
	private int getColorCode(AlarmLimitValue alv) {
		if (alv!=null && alv.getLimit()!=null) {
			return alv.getLimit().colorCode();
		}
		else {
			return App.UNKNOWN_COLORCODE;
		}
	}
	
	/**
	 * You must call this if the data is timed out. All data will be cleared, but name and id
	 * will remain.
	 */
	public void invalidate() {
		dtg=null;
		ff=null;
		dd=null;
		fx=null;
	}
	
	public HslData clone() throws CloneNotSupportedException {
	  return (HslData)super.clone();
	}

} //end class
