package meteoconsult.products.hsl.chart;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;

import meteoconsult.database.tables.Table;
import meteoconsult.database.tables.meteo.key.FcstKey;
import meteoconsult.lib.common.chart.CustomXYChart;
import meteoconsult.products.hsl.App.AlarmLimit;
import meteoconsult.products.hsl.limit.AlarmLimitValue;
import meteoconsult.products.hsl.limit.DegreesLimitStore;
import meteoconsult.products.hsl.limit.StationLimits;
import meteoconsult.util.ColorUtil;
import ChartDirector.Chart;
import ChartDirector.Layer;
import ChartDirector.LineLayer;


/**
 * CustomXYChart with presettings for short term
 */
public class HslWindChart extends CustomXYChart  {
	
	protected final static String DEFAULT_HOUR_FORMAT = "{value|w hh:nn}";
	private StationLimits sl;
  private ArrayList<Double> legendDegrees = null;
  private ArrayList<Double> legendDates = null;
	
	public HslWindChart(int chartWidth,int chartHeight, TimeZone tz, StationLimits sl) {
		super(chartWidth,chartHeight);
		setTimeZone(tz);
		this.sl = sl;
		//setBackground(0xd0a0a0);
		setWeekDayNames(CustomXYChart.WEEKDAYS_NL);
		setGridLines(CustomXYChart.DOUBLE_HOURLY_GRIDLINES);
		xAxis().setLabelFormat(DEFAULT_HOUR_FORMAT);
		yAxis2().setTitle("wind [kts]","plain");
		//setPlotArea(24, 0, chartWidth-50, chartHeight-30);
		setPlotArea(24, 0, chartWidth-48, chartHeight-30);
		setFillGap(false);
	} //end constructor
	
	 /**
   * Adds a Line to the given chart, based on the table. and the PLUS_HOURS and MIN_HOURS.
   *
   * @param c XYChart
   * @param key FcstKey
   * @param table Table
   * @param dates Date[]
   */
  protected Layer addLine(FcstKey key, Table table, Date[] dates) {
    if (key == null) {
      log.severe("addLine: key is null");
    }
    if (key.dbKey == null) {
      log.severe("addLine: key.dbKey is null");
    }
    String name = (key.name == null) ? "" : key.name;
    int iColor = ColorUtil.toInt(key.getColor());
    double[] vals = getNumericalData(dates,table, key);
    
    //add an extra empty point if 'now' is not in the graph
    if ( dates.length>0 ) {
    	//Date now = new Date();
    	Date now = adjustToTimeZone(new Date());
    	
    	Date dtFirst = dates[0];
    	Date dtLast = dates[dates.length-1];
    	if (dtFirst.after(now)) {
    		//add 'now' point in front
		    Date[] plusDates = new Date[dates.length+1];
		    double[] plusVals = new double[vals.length+1];
		    plusDates[0] = now;
		    plusVals[0]= Chart.NoValue;
		    for (int i=0;i<dates.length;i++) {
		    	plusDates[i+1]=dates[i];
		    	plusVals[i+1]=vals[i];
		    }
		    dates=plusDates;
		    vals=plusVals;
		    
		    //log.info("add to the front: "+dtFirst+"   dtLast: "+dtLast+"   dtNow: "+now);
    	}
    	if (dtLast.before(now)) {
    		//add 'now' point at the end
		    Date[] plusDates = new Date[dates.length+1];
		    double[] plusVals = new double[vals.length+1];
		    for (int i=0;i<dates.length;i++) {
		    	plusDates[i]=dates[i];
		    	plusVals[i]=vals[i];
		    }
		    plusDates[plusDates.length-1] = now;
		    plusVals[plusVals.length-1]= Chart.NoValue;
		    dates=plusDates;
		    vals=plusVals;
		    //log.info("add to the end dtFirst: "+dtFirst+"   dtLast: "+dtLast+"   dtNow: "+now);
    	} //dtFinal after dtLast
    }
    if (dates.length != vals.length) {
      log.warning("addLine: incompatible quantities, found " +
          dates.length + " dates with " + vals.length + " values");
    }

    LineLayer line = addLineLayer(vals, iColor, name);
    if (fillGap){
      line.setGapColor(dashLineColor(iColor,Chart.DashLine));
    }
    line.setXData(dates);
    line.setLineWidth(2);
    
    adjustOrientation(line, key);
    adjust(vals,key);
    //adjust(line, vals, key);
    return line;
  } //end addLine
  
  /**
   * Returns a graph with wind
   * @param table Table
   * @param wind1 FcstKey  the key for wind 1 (in Knots)
   * @param wind2 FcstKey  the key for wind 2 (in Knots, may be null)
   * @param dir FcstKey    the key for the wind direction (may be null)
   * @param dateKey FcstKey  the key for the date (DTG)
   * @return XYChart
   */
  public void addWindKeys(Table table, FcstKey wind1, FcstKey wind2, FcstKey dir, FcstKey dateKey) {
    Date[] dates = getDates(table, dateKey.dbKey);
    //add left
    addLine(wind1, table, dates);
    if (wind2 != null) {
    	addLine(wind2, table, dates);
    }

    //the direction arrows
    if (dir != null) {
      String nameD = (dir.name == null) ? "" : dir.name;
      int iColorD = ColorUtil.toInt(dir.getColor());
      
      double[] origDegrees = getNumericalData(dates,table, dir);
      //skip degrees if there are more then 35 to fit in the graph and check on risky winddirs
      ArrayList<Double> normalDegrees = new ArrayList<Double>();
      ArrayList<Double> normalDates = new ArrayList<Double>();
      ArrayList<Double> warnDegrees = new ArrayList<Double>();
      ArrayList<Double> warnDates = new ArrayList<Double>();
      legendDegrees = new ArrayList<Double>();
      legendDates = new ArrayList<Double>();      
     
      int skipDegrees = (int)Math.ceil(origDegrees.length/35.0d);
  		for (int d=0;d<origDegrees.length;d++) {
        if ( (d%skipDegrees==0)&&(origDegrees[d]!=Chart.NoValue)) {
        	if ( (origDegrees[d]>=225)&&(origDegrees[d]<=275) ) {
        		warnDegrees.add(origDegrees[d]-180); //inverse direction 
        		warnDates.add(Chart.CTime(dates[d]));
        	}
        	else {
        		normalDegrees.add(origDegrees[d]-180); //inverse direction 
        		normalDates.add(Chart.CTime(dates[d]));
        	}
        	legendDegrees.add(origDegrees[d]);
        	legendDates.add(Chart.CTime(dates[d]));
        	
  		  } //if noot skip
  		} //for v
  		
  		//add normal arrows
  		if (normalDates.size()>0) {
	      double[] xVals = new double[normalDates.size()];
	      double[] valsW1 = new double[normalDates.size()];
	      double[] lengths = new double[normalDates.size()];
	      double[] degrees = new double[normalDates.size()];
	      for (int i = 0; i < normalDates.size(); i++) {
	      	xVals[i] = normalDates.get(i);
	        lengths[i] = 15;
	        valsW1[i]=maxYLeft;
	        degrees[i]=normalDegrees.get(i);
	      }
  			addVectorLayer(xVals, valsW1, lengths, degrees,Chart.PixelScale, iColorD, nameD);  
  		}
  		
  		//add WARNING arrows
  		if (warnDates.size()>0) {
	      double[] xVals = new double[warnDates.size()];
	      double[] valsW1 = new double[warnDates.size()];
	      double[] lengths = new double[warnDates.size()];
	      double[] degrees = new double[warnDates.size()];
	      for (int i = 0; i < warnDates.size(); i++) {
	      	xVals[i] = warnDates.get(i);
	        lengths[i] = 15;
	        valsW1[i]=maxYLeft;
	        degrees[i]=warnDegrees.get(i);
	      }
  			addVectorLayer(xVals, valsW1, lengths, degrees,Chart.PixelScale, 0xFF0000, nameD);  
  		}
      maxYLeft=maxYLeft+5; //raise the left scale to get more space for the arrows
    } //dir not null
    
  } //end getWindKeys
  

  public final static int THICKNESS = 1;

  /**
   * Modified to add legends
   */
  public void defaultLayout() {
  	//wws legend must be done before the layout, these are lines, not zones
		DegreesLimitStore wws = sl.getLimitStore();
		if (wws!=null && legendDegrees!=null && legendDates!=null) {
			//prepare	
			double[] dateArray = new double[legendDates.size()];
			for (int i=0;i<legendDates.size();i++) {
				dateArray[i] = legendDates.get(i).doubleValue();
			}
			HashMap<AlarmLimit,double[]> degreeLimitMatrix = new HashMap<AlarmLimit,double[]>();
								
			//loop through the degrees to fill the matrix
			for (int d=0; d<legendDegrees.size();d++) {
				Double degree = legendDegrees.get(d);
				AlarmLimitValue[] wwsLimits = wws.getLimits(degree.intValue());
				if (wwsLimits!=null) {
					for (AlarmLimitValue wwsLimit : wwsLimits) {
						AlarmLimit limit = wwsLimit.getLimit();
						double[] vals = degreeLimitMatrix.get(limit);
						if (vals==null) {
							vals = new double[legendDegrees.size()];
							degreeLimitMatrix.put(limit, vals);
						}
	  				vals[d] = wwsLimit.getValue();
	  			} //for bridgeLimit
				} //wwsLimits!=null;
			} //for degree

			for (AlarmLimit wwsLimit : degreeLimitMatrix.keySet()) {
				double vals[] = degreeLimitMatrix.get(wwsLimit);
				LineLayer line = addLineLayer(vals, wwsLimit.colorCode());
				line.setGapColor(dashLineColor(wwsLimit.colorCode(),Chart.Transparent));
				line.setXData(dateArray);
				line.setLineWidth(THICKNESS);
			} //for wwsLimit
		} //wws not null
		
		//do the layout to get the maxYLeft
  	super.defaultLayout();
  	
		double maxY = yAxis().getMaxValue();
  	//add legends
  	if (sl!=null) {
//  		AlarmLimitValue[] northLimits = sl.getNorthLimits();
//  		if (northLimits!=null) {
//  			for (AlarmLimitValue limit : northLimits) {
//  				if (maxY>=limit.getValue()) {
//  					yAxis().addZone(limit.getValue(),limit.getValue()+THICKNESS , limit.getLimit().colorCode());
//  				}
//  			} //for northLimit
//  		} //north
//
//  		AlarmLimitValue[] southLimits = sl.getSouthLimits();
//  		if (southLimits!=null) {
//  			for (AlarmLimitValue limit : southLimits) {
//  				if (maxY>=limit.getValue()) {
//  					yAxis().addZone(limit.getValue(),limit.getValue()+THICKNESS , limit.getLimit().colorCode());
//  				}
//  			} //for southLimit
//  		} //south

  		AlarmLimitValue[] bridgeLimits = sl.getBridgeLimits();
  		if (bridgeLimits!=null) {
  			for (AlarmLimitValue limit : bridgeLimits) {
  				if (maxY>=limit.getValue()) {
  					yAxis().addZone(limit.getValue(),limit.getValue()+THICKNESS , limit.getLimit().colorCode());
  				}
  			} //for bridgeLimit
  		} //bridge 		
  		
  	} //sl not null
	} //defaultLayout
	
	
} //end class HslWindChart

