package meteoconsult.products.hsl.servlet;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.awt.RenderingHints;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import meteoconsult.products.hsl.App;
import Acme.JPM.Encoders.GifEncoder;

/**
 *	Returns an image with the given value and degrees
 */
public class WindPicServlet extends HttpServlet {
	private static final long serialVersionUID = -7778916238542552983L;
	public final Logger log = Logger.getLogger( getClass().getName() );


	private final static Font DEFAULT_FONT = new Font("Arial,SansSerif",Font.BOLD,14);
	private final static DecimalFormat DECIMAL = new DecimalFormat("0");


	//Process the HTTP Get request
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("image/png");

		//caching is allowed
		Integer degrees = null;
		Double value = null;

		String sDegrees = request.getParameter("degrees");
		String sValue = request.getParameter("value");
		try {
			//			Integer edgeColor = null;
			//			if (sEdgeColor!=null) edgeColor = Integer.parseInt(sEdgeColor);

			if (sDegrees!=null && sDegrees.length()>0 ) degrees = new Integer(sDegrees);
			if (sValue!=null && sDegrees.length()>0 ) value = new Double(sValue);

			String sWindColor = (request.getParameter("windcolor"));
			Integer windColor = null;
			if (sWindColor!=null) windColor = Integer.parseInt(sWindColor);
			Color background = (windColor==null) ? new Color(App.UNKNOWN_COLORCODE) : new Color(windColor);

			String sPreAlarmColor = (request.getParameter("edgecolor"));
			Integer preAlarmColor = null;
			if (sPreAlarmColor!=null) preAlarmColor = Integer.parseInt(sPreAlarmColor);
			Color preAlarmCol = (preAlarmColor==null) ? new Color(App.UNKNOWN_COLORCODE) : new Color(preAlarmColor);

			//Color background = null;
			response.getOutputStream().write(windImage(degrees,value, preAlarmCol, background,request.getParameter("type")) );
		} //try
		catch (NumberFormatException ex) {
			//log.info("parameter 'degrees' or 'value' is invalid, params = "+request.getQueryString());
		}
	}


	/**
	 * Generates an Image saying that no data is available
	 * @return
	 */
	private byte[] windImage(Integer degrees, Double value, Color preAlarmColor, Color background, String type) {
		int width=34;	
		int height = width;
		//degrees = (degrees-90)%360; //compensate for wind direction

		int centX = Math.round(width/2.0f);
		int centY =  Math.round(height/2.0f);

		// Create a buffered image in which to draw
		BufferedImage bufferedImage = new BufferedImage(width,height, BufferedImage.TYPE_INT_ARGB);
		// Create a graphics contents on the buffered image
		Graphics2D g2d = bufferedImage.createGraphics();
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);

		if (value!=null) {
			float radius = centX+1;

			//draw the arrow
			double rad0 = Math.toRadians(degrees%360);
			double rad1 = Math.toRadians((degrees+125)%360);
			double rad3 = Math.toRadians((degrees+235)%360);

			//arrowpoint
			double zeroX = centX-(radius*Math.sin(rad0));
			double zeroY = centY+(radius*Math.cos(rad0));

			//wind direction (mirrored 0 = VnorthV, 90 = <east<, 180= ^south^, 270 = >west>
			Polygon poly = new Polygon();
			poly.addPoint( (int) (zeroX), (int) (zeroY) );
			poly.addPoint( (int) (centX-(radius*Math.sin(rad1))), (int) (centY+(radius*Math.cos(rad1))) );
			poly.addPoint( (int) (centX-(radius*Math.sin(rad3))), (int) (centY+(radius*Math.cos(rad3))) );
			poly.addPoint( (int) (zeroX), (int) (zeroY) );		


			//add line
			if (App.TYPE_1_min.equals(type)) {
				//draw all in colour for the 10% pre-alarm limit, do not show the 'normal' colour anymore for WWS stations
				if (preAlarmColor!=null) {
					g2d.setColor(preAlarmColor);
					g2d.fillPolygon(poly);
				}

				//draw the arrowpoint for type1
				if (App.TYPE_1_min.equals(type)) {
					int[] xPoints = { 
							(int) (centX - (radius * Math.sin(rad3))),
							(int) (zeroX),
							(int) (centX - (radius * Math.sin(rad1)))
					};

					int[] yPoints = {
							(int) (centY+(radius*Math.cos(rad3))),
							(int) (zeroY),
							(int) (centY+(radius*Math.cos(rad1)))	
					};
					g2d.setStroke(new BasicStroke(2));
					g2d.setColor(Color.BLACK);
					g2d.drawPolyline(xPoints, yPoints, 3);
				} //draw arrowpoint for type 1
			} //TYPE_1_min
			else {
				//draw all others in the limit colour
				if (preAlarmColor!=null) {
					g2d.setColor(preAlarmColor);
					g2d.fillPolygon(poly);
				}
			}
		} //value not null


		//text
		if (degrees==null) {
			degrees=0;
		}
		double rad = Math.toRadians(degrees%360);
		//double rad2 = Math.toRadians((degrees+180)%360);
		double textXMove = (Math.sin(-rad)*width/10.0f) ; //locate text max 3.5 pix left or right
		double textYMove = (Math.cos(rad)*width/10.0f) ; //locate text max 3.5 pix left or right

		FontMetrics fm = g2d.getFontMetrics(DEFAULT_FONT);
		String sValue = (value==null) ? "?" : DECIMAL.format(value.doubleValue());
		Rectangle2D rect = fm.getStringBounds(sValue,g2d);
		double x = (width/2.0)-rect.getCenterX()-textXMove;
		double y = (height/2.0)-rect.getCenterY()-textYMove;
		g2d.setColor(Color.BLACK);
		g2d.setFont(DEFAULT_FONT);
		g2d.drawString(sValue,(int)x,(int)y);

		//destination coordinates
		ByteArrayOutputStream bos = new ByteArrayOutputStream(width*height);
		try {
			GifEncoder encoder = new GifEncoder(bufferedImage,bos);
			encoder.encode();
		}
		catch (IOException ex) {
			ex.printStackTrace();
		}
		// Graphics context no longer needed so dispose it
		g2d.dispose();


		return bos.toByteArray();
	}

} //end class
