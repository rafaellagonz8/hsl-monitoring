package meteoconsult.products.hsl.servlet;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import meteoconsult.products.common.html.JSPTemplates;
import meteoconsult.products.hsl.App;
import meteoconsult.products.hsl.HslDataProvider;


/**
 * Triggers the refresh of the Dataprovider if invoked by an AJAX request 
 */
public class DataProviderServlet extends HttpServlet {
	//constants
	private static final long serialVersionUID = -2720243817720840113L;

	//private Date lastDtgCheck = null;

	/**
	 * Invokes a change on the dataprovider, does not return anything
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String sType = request.getParameter("type");
		
		response.setContentType("text/plain");
		response.setHeader("Cache-Control","no-cache"); //HTTP 1.1
		response.setHeader("Pragma","no-cache"); //HTTP 1.0
		response.setDateHeader ("Expires", 0); //prevents caching at the proxy server		
		
		
		
		//App.log.info("DataProviderServlet sType: "+sType+"  params: "+request.getQueryString());
		if (sType==null) return;

		if (sType.equalsIgnoreCase("refresh")) {
			HslDataProvider dataProvider = (HslDataProvider)request.getSession().getAttribute("dataProvider");
			if (dataProvider!=null) {
				Date dtg=JSPTemplates.getDate("dtg", request);
				if (dtg==null) {
					App.log.warning("no dtg defined, could not return minute data");
					return;
				}		
				Integer interval = JSPTemplates.getInteger("interval",request);
				if (interval==null) {
					App.log.warning("no interval defined, could not return minute data");
					return;
				}
				Calendar calMin = Calendar.getInstance();
				calMin.setTime(dtg);
				calMin.add(Calendar.MINUTE,-interval);
				dataProvider.refresh(calMin.getTime(), dtg);
			}
		} //if refresh
		
		if (sType.equalsIgnoreCase("station")) {
			HslDataProvider dataProvider = (HslDataProvider)request.getSession().getAttribute("dataProvider");
			if (dataProvider!=null) {
				Integer station = JSPTemplates.getInteger("id",request);
				//App.log.info("station selected: "+station);
				if (station==null) {
					App.log.warning("no station defined");
					return;
				}
				else {
					dataProvider.setSelectedStation(station);
				}
				//dataProvider.refresh(calMin.getTime(), dtg);
			}
		} //if station

	} //end doGet

	

} //end class
