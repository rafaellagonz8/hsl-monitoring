package meteoconsult.products.hsl.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import meteoconsult.products.common.html.JSPTemplates;
import meteoconsult.util.DateTime;


/**
 * Returns the fresh dtg's for HSL.
 * 
 * Any request will execute a db query. Use a static loader if a lot of concurrent users may
 * question this servlet for performance reasons.
 */
public class DtgServlet extends HttpServlet {
	//constants
	private static final long serialVersionUID = -2720243817720840113L;
	public static Logger log = Logger.getLogger(DtgServlet.class.getName());
	
	private final static int HOURS_HISTORY = 24;

	
	private static final String LBL_NAME = "name";
	private static final String LBL_VALUE = "value";
	
  //Process the HTTP Get request
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String sType = request.getParameter("type");
		//log.info("sType: "+sType);
		if (sType==null) return;

		if (sType.equalsIgnoreCase("dtgs")) {
			response.setContentType("text/xml");
			response.setHeader("Cache-Control","no-cache"); //HTTP 1.1
			response.setHeader("Pragma","no-cache"); //HTTP 1.0
			response.setDateHeader ("Expires", 0); //prevents caching at the proxy server		
			PrintWriter out = response.getWriter();
			//out.flush();
			out.println(dtgXML(request));
			return;
		} //if dtgs
	}

	//-------------dtg methods-----------

	/**
	 * Returns the data for the DTG selecter
	 * @param tz
	 * @return
	 */
	private StringBuffer dtgXML(HttpServletRequest request) {
		Integer interval=JSPTemplates.getInteger("interval",request);

		StringBuffer sb = new StringBuffer();
		//user stations required

		TimeZone tz = DateTime.TIMEZONE_NL; //fixed
		
		SimpleDateFormat sdfDisplay = new SimpleDateFormat(DateTime.DB_YEAR_TO_MIN);
		sdfDisplay.setTimeZone(tz);

		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.MILLISECOND, 0);
		cal.set(Calendar.SECOND, 0);
		Date start = cal.getTime();
		
	
		Calendar minCal = (Calendar)cal.clone();
		minCal.add(Calendar.HOUR,-HOURS_HISTORY);
		Date minTime = minCal.getTime();
		
		//round up to 5 min
		int minutes = cal.get(Calendar.MINUTE);
		
		int minutesRoundUp = (int)(5 * Math.ceil(((minutes-interval+5)/5.0)) );
		cal.set(Calendar.MINUTE,0);
		cal.add(Calendar.MINUTE,minutesRoundUp);
		Date dtg = cal.getTime();

		sb.append("<?xml version='1.0'?>");
		sb.append("<data>");
		sb.append("<dtgs>");

		sb.append("<dtg name='"+sdfDisplay.format(start)+" (latest)' value='"+DateTime.SDF_YEAR_TO_MIN.format(start)+"'></dtg>");
		
		while (dtg.after(minTime)) { 
			sb.append("<dtg name='"+sdfDisplay.format(dtg)+"' value='"+DateTime.SDF_YEAR_TO_MIN.format(dtg)+"' ></dtg>");
			cal.add(Calendar.MINUTE,-1*interval);
			dtg = cal.getTime();
		} //for stations
		sb.append("</dtgs>");
		sb.append("</data>");
		return sb;
	}

} //end class
