package meteoconsult.products.hsl.servlet;

import static meteoconsult.products.hsl.App.FLAG_BRIDGE;
import static meteoconsult.products.hsl.App.FLAG_NORTH;
import static meteoconsult.products.hsl.App.FLAG_SOUTH;
import static meteoconsult.products.hsl.App.FLAG_WWS;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import meteoconsult.products.hsl.App;
import meteoconsult.products.hsl.HslData;
import meteoconsult.products.hsl.HslDataProvider;
import meteoconsult.products.hsl.limit.AlarmLimitValue;
import meteoconsult.products.hsl.limit.StationLimits;
import meteoconsult.util.DateTime;
import meteoconsult.util.Display;
import flexjson.JSONSerializer;

/**
 * Gives the locations and recent data for hsl stations
 * 
 * Any request will execute a db query. Use a static loader if a lot of concurrent users may
 * question this servlet for performance reasons.
 */
public class MapDataServlet extends HttpServlet {
	//constants
	private static final long serialVersionUID = -2720243817720840113L;
	public static Logger log = Logger.getLogger(MapDataServlet.class.getName());

	//private Date lastDtgCheck = null;

//	Process the HTTP Get request
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String sType = request.getParameter("type");
		if (sType==null) return;

		if (sType.equalsIgnoreCase("mapdata")) {
			response.setContentType("text/xml");
			response.setHeader("Cache-Control","no-cache"); //HTTP 1.1
			response.setHeader("Pragma","no-cache"); //HTTP 1.0
			response.setDateHeader ("Expires", 0); //prevents caching at the proxy server		
			PrintWriter out = response.getWriter();
			out.println(dataXML(request));
			return;
		} //if dtgs
		else if (sType.equalsIgnoreCase("stationdata")) {
			response.setContentType("text/xml");
			response.setDateHeader ("max-age", 600); //allows limited	caching (in secs)
			PrintWriter out = response.getWriter();
			out.println(stationXML(request));
			return;
		} //if dtgs
	}
	
	/**
	 * Returns the station attributes like name lat lon etc
	 * @param tz
	 * @return
	 */
	private StringBuffer stationXML(HttpServletRequest request) {
		HslDataProvider dataProvider = (HslDataProvider)request.getSession().getAttribute("dataProvider");
		StringBuffer xml = new StringBuffer();
		if (dataProvider!=null) {
			xml.append("<?xml version='1.0'?>");
			xml.append("<data>");
			xml.append("<hsl_minute_data>");
      xml.append("<hsl_stations>");
      for (HslData data: 	dataProvider.get1MinDatas()) {
      	int statId = data.getStatId();
      	StationLimits sl = App.getLimitProvider().getStationsLimit(statId);
      	xml.append("<station "
					+"id='"+data.getStatId()+"' "
					+"name='"+Display.getDefault(data.getName(),"")+"' "
					+"lat='"+data.getLat()+"' "
					+"lon='"+data.getLon()+"' "
					+"type='1min' "
					+ ( (sl==null) ? "" : "flag='"+sl.getFlag()+"' ")
					+">");
      	xml.append("</station>");
      }
      for (HslData data: 	dataProvider.get10MinDatas()) {
      	int statId = data.getStatId();
      	StationLimits sl = App.getLimitProvider().getStationsLimit(statId);
      	xml.append("<station "
					+"id='"+data.getStatId()+"' "
					+"name='"+Display.getDefault(data.getName(),"")+"' "
					+"lat='"+data.getLat()+"' "
					+"lon='"+data.getLon()+"' "
					+"type='10min' "
					+ ( (sl==null) ? "" : "flag='"+sl.getFlag()+"' ")
					+">");
      	xml.append("</station>");
      }
      xml.append("</hsl_stations>");
			xml.append("</hsl_minute_data>");
			//flag definition
			xml.append("<flags>");
			xml.append("<flag id='"+FLAG_NORTH+"' descr='north' />");
			xml.append("<flag id='"+FLAG_SOUTH+"' descr='south' />");
			xml.append("<flag id='"+FLAG_BRIDGE+"' descr='bridge' />");
			xml.append("<flag id='"+FLAG_WWS+"' descr='wws' />");
			xml.append("</flags>");
						
			xml.append("</data>");
		} //dataProvider not null
		return xml;
	}

	
	/**
	 * Returns the recent station data
	 * @param tz
	 * @return
	 */
	private StringBuffer dataXML(HttpServletRequest request) {
		HslDataProvider dataProvider = (HslDataProvider)request.getSession().getAttribute("dataProvider");
		
		//JSONSerializer serializer = new JSONSerializer().exclude("class");
		
		StringBuffer xml = new StringBuffer();
		if (dataProvider!=null) {
			xml.append("<?xml version='1.0'?>");
			xml.append("<data>");
			xml.append("<hsl_minute_data>");
      xml.append("<hsl_stations>");
      for (HslData data: 	dataProvider.get1MinDatas()) {
      	xml.append("<station "
					+"id='"+data.getStatId()+"' "
					+"dtg='"+( (data.getDtg()==null) ? "" : DateTime.SDF_YEAR_TO_MIN.format(data.getDtg()) )+"' "
					+"ff='"+Display.getDefault(data.getFf(),"")+"' "
					+"dd='"+Display.getDefault(data.getDd(),"")+"' "
					+"fx='"+Display.getDefault(data.getFx(),"")+"' "
					+"colorff='"+data.getFfColorCode()+"' "
					+"colorfx='"+data.getFxColorCode()+"' "
					+"colorffPre='"+getPreColorCode(data.getPreAlarmLimitFf())+"' "
					+"colorfxPre='"+getPreColorCode(data.getPreAlarmLimitFx())+"' "
					+"type='"+App.TYPE_1_min+"' "
					+">");
      	xml.append("</station>");
      }
      for (HslData data: 	dataProvider.get10MinDatas()) {
      	xml.append("<station "
					+"id='"+data.getStatId()+"' "
					+"dtg='"+( (data.getDtg()==null) ? "" : DateTime.SDF_YEAR_TO_MIN.format(data.getDtg()) )+"' "
					+"ff='"+Display.getDefault(data.getFf(),"")+"' "
					+"dd='"+Display.getDefault(data.getDd(),"")+"' "
					+"fx='"+Display.getDefault(data.getFx(),"")+"' "
					+"colorff='"+data.getFfColorCode()+"' "
					+"colorfx='"+data.getFxColorCode()+"' "
					+"colorffPre='"+getPreColorCode(data.getPreAlarmLimitFf())+"' "
					+"colorfxPre='"+getPreColorCode(data.getPreAlarmLimitFx())+"' "
					+"type='"+App.TYPE_10_min+"' "
					+">");
      	xml.append("</station>");
      }
      xml.append("</hsl_stations>");
			xml.append("</hsl_minute_data>");
			xml.append("</data>");
			
		} //dataProvider not null
		return xml;
	}
	
	
	/**
	 * convenience method to get access to the AlarmLimit.colorCode
	 * @return
	 */
	protected int getPreColorCode(AlarmLimitValue alv) {
		if (alv!=null && alv.getLimit()!=null) {
			return alv.getLimit().colorCode();
		}
		else {
			return 0xEEEEEE;
		}
	}
	
	
	/**
	 * Returns the recent station data
	 * @param tz
	 * @return
	 */
	private StringBuffer dataJSON(HttpServletRequest request) {
		HslDataProvider dataProvider = (HslDataProvider)request.getSession().getAttribute("dataProvider");
		
		JSONSerializer serializer = new JSONSerializer().exclude("class");
		StringBuffer json = new StringBuffer();
		if (dataProvider!=null) {
			json.append(serializer.serialize( dataProvider.get1MinDatas() ) );
			json.append(serializer.serialize( dataProvider.get10MinDatas() ) );
		}
		return json;
	}


} //end class
