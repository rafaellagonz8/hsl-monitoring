package meteoconsult.products.hsl.servlet;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.IOException;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TimeZone;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import meteoconsult.database.tables.Table;
import meteoconsult.database.tables.meteo.Mg_forecast;
import meteoconsult.database.tables.meteo.key.FcstKey;
import meteoconsult.database.tables.meteo.key.FcstKeyable;
import meteoconsult.database.tables.meteo.key.FcstKeys;
import meteoconsult.products.common.html.JSPTemplates;
import meteoconsult.products.hsl.App;
import meteoconsult.products.hsl.HslData;
import meteoconsult.products.hsl.HslDataProvider;
import meteoconsult.products.hsl.chart.HslWindChart;
import meteoconsult.products.hsl.limit.StationLimits;
import meteoconsult.util.DateTime;

import org.apache.log4j.Logger;

import ChartDirector.Chart;
import ChartDirector.TextBox;
import ChartDirector.XYChart;


/**
 * The chart for HSL obs and Fcst
 *
 */
public class StationGraphServlet extends HttpServlet {
	private static final long serialVersionUID = -7850300103226079149L;
	public final static Logger log = Logger.getLogger(StationGraphServlet.class);  
	private final static int CHART_WIDTH = 717;
	private final static int CHART_HEIGHT =155;
	
	//private final static long PLUS_HOURS  =  10 * DateTime.MINUTE;
	//private final static long MINUS_HOURS = 6 * DateTime.HOUR;
	
	/* cache the used charts, to prevent threading problems, synchronize this map otherwise it may grow over the defined size */
	private static Map<Table,XYChart> cacheMap= Collections.synchronizedMap(new LinkedHashMap<Table,XYChart>() {
		@Override
		protected boolean removeEldestEntry(Map.Entry<Table,XYChart> eldest) {
			return size() > 3;
		}		
	});
	
	
	public static Map<Table,XYChart> getCacheForTest() {
		return cacheMap;
	}

	
	private static TimeZone tz = DateTime.TIMEZONE_NL;
	
	
  //Process the HTTP Get request
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		boolean bOK = false;
		String type = request.getParameter("type");
		
		
		
		response.setContentType("image/png");
		Integer stationId = JSPTemplates.getInteger("station_id",request);
		HslDataProvider dataProvider = (HslDataProvider)request.getSession().getAttribute("dataProvider");

		if (dataProvider!=null) {
			HslData station = null;

			if (stationId!=null) {
				station = dataProvider.getHslData(stationId);
			}
			else {
				station = dataProvider.getSelectedHslData();
				if (station!=null) {
					stationId = station.getStatId();
				}
			}
			
			if (station!=null) {
				
				if (type.equalsIgnoreCase("fcst")) {
					String title = ("fcst - ("+station.getStatId()+") "+station.getName()).toUpperCase();
					Mg_forecast dbTable = dataProvider.getBridgeFcstData();
					//first try to get from cache
					XYChart windChart = cacheMap.get(dbTable);	
					if ( (windChart==null) && (dbTable!=null) && (dbTable.size()>0) ) {	
						//resolve the timezone from the SessionBean 'generalBean'
						//create the chart
						StationLimits sl = App.getLimitProvider().getStationsLimit(stationId);
						windChart = createWindChart(dbTable,tz,title,sl);
						cacheMap.put(dbTable,windChart);
						bOK=true;
					} //at least some data
					response.getOutputStream().write(windChart.makeChart(Chart.PNG));
				} //if fcst
				else {	
					String title = ("obs - ("+station.getStatId()+") "+station.getName()).toUpperCase();
					Table dbTable;
					if ( station.getStatId()==App.ID_HSL_BRIDGE) {
						dbTable = dataProvider.getBridgeGraphData();
					}
					else {
						dbTable = dataProvider.getVarGraphData();
					}			
					
					//create the chart
					XYChart windChart = cacheMap.get(dbTable);	
					if (windChart==null) {
						StationLimits sl = App.getLimitProvider().getStationsLimit(stationId);
						windChart = createWindChart(dbTable,tz,title,sl);
						cacheMap.put(dbTable,windChart);
					}
					response.getOutputStream().write(windChart.makeChart(Chart.PNG));
					bOK=true;
				} //else (=not MOS)
			} //if station not null
	
		} //dataProvider not null
		if (!bOK) {
			ImageIO.write(noDataImage(CHART_WIDTH, CHART_HEIGHT), "png", response.getOutputStream());
		} //not OK
	}
	
	/**
	 * Creates a temperature chart based on the DbTable
	 * @return XYChart
	 */
	protected XYChart createWindChart(Table dbTable, TimeZone tz,String title, StationLimits sl) {
		if ( (dbTable==null) || (!(dbTable instanceof FcstKeyable)) )
			App.log.severe("dbTable is null or does not implement FcstKeyable");

		FcstKeyable fkTable = (FcstKeyable)dbTable;
		FcstKeys tableKeys = fkTable.getFcstKeys();

		tableKeys.WIND_10M_KNOTS.name="windspeed";
		tableKeys.WIND_10M_KNOTS.color= Color.BLUE;
		
		tableKeys.GUSTS_10M_KNOTS.name="winds gust";
		tableKeys.GUSTS_10M_KNOTS.color= Color.RED;
		
		tableKeys.WIND_10M_DIRECTION.name="wind dir.";
		tableKeys.WIND_10M_DIRECTION.color= Color.BLACK;
		tableKeys.WIND_10M_DIRECTION.orientation = FcstKey.ORIENTATION_RIGHT;
		
		HslWindChart chart = new HslWindChart(CHART_WIDTH,CHART_HEIGHT,tz, sl);
		//chart.addDateKeys(dbTable, keys, tableKeys.DTG);
		chart.addWindKeys(dbTable,tableKeys.WIND_10M_KNOTS,tableKeys.GUSTS_10M_KNOTS
				,tableKeys.WIND_10M_DIRECTION,tableKeys.DTG);
		chart.setThreshold(new Double(0.0));
		TextBox tTitle = chart.addTitle(title,"plain",8);
		tTitle.setMargin2(0,0,5,0);
		chart.defaultLayout();
		return chart;
	} //end windObsChart()
	

	/**
	 * Generates an Image saying that no data is available
	 * @return
	 */
	private RenderedImage noDataImage(int width, int height) {
		// Create a buffered image in which to draw
		BufferedImage bufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);

		// Create a graphics contents on the buffered image
		Graphics2D g2d = bufferedImage.createGraphics();

		// Draw graphics
		g2d.setColor(Color.white);
		g2d.fillRect(0, 0, width, height);
		g2d.setColor(Color.black);
		g2d.drawString("no data available",5,15);

		// Graphics context no longer needed so dispose it
		g2d.dispose();
		return bufferedImage;
	}
 
	
} //end class