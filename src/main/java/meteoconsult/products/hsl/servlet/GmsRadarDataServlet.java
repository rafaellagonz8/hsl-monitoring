package meteoconsult.products.hsl.servlet;

import javax.servlet.ServletException;

import meteoconsult.products.common.maps.radar.RadarDataServlet;
import meteoconsult.util.DateTime;

public class GmsRadarDataServlet extends meteoconsult.lib.common.servlet.ProxyServlet {
	private static final long serialVersionUID = -7181018781148044141L;
	
	public void init() throws ServletException {
		remoteUrl		= REMOTE_RADAR_URL;
		servletName	=	"nlRadarTypeServlet";
	}	
	
	protected String addAddtionalParameters(String sUrl) {
		sUrl	= addParameter(sUrl, RadarDataServlet.PARAM_CACHEONLY, false);
		sUrl	= addParameter(sUrl, RadarDataServlet.PARAM_OBSDELTA, 120);
		sUrl	= addParameter(sUrl, RadarDataServlet.PARAM_FCSTDELTA, 120);
		sUrl	= addParameter(sUrl, RadarDataServlet.PARAM_TIMEZONE, DateTime.TIMEZONE_NL.getID());
		
		return sUrl;
	}
} //end class



