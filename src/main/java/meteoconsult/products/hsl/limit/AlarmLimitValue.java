package meteoconsult.products.hsl.limit;

import meteoconsult.products.hsl.App.AlarmLimit;

public class AlarmLimitValue {
	private AlarmLimit limit;
	private int value;
	
	public AlarmLimitValue(AlarmLimit limit, int value) {
		this.limit = limit;
		this.value = value;
	}
	
	public AlarmLimit getLimit() {
		return limit;
	}

	public void setLimit(AlarmLimit limit) {
		this.limit = limit;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}
	
} //end class
