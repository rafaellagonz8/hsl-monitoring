package meteoconsult.products.hsl.limit;

import static meteoconsult.products.hsl.App.FLAG_BRIDGE;
import static meteoconsult.products.hsl.App.FLAG_WORST;
import static meteoconsult.products.hsl.App.FLAG_WWS;

/**
 *
 */
public class StationLimits {
	
	private DegreesLimitStore limitStore;
	private AlarmLimitValue[] worstLimits;
	private AlarmLimitValue[] bridgeLimits;
	
	
	public StationLimits() {
	}

	
	public void setLimitStore(DegreesLimitStore wws) {
		this.limitStore = wws;
	}
	public DegreesLimitStore getLimitStore() {
		return limitStore;
	}
	
//	public void setNorthLimits(AlarmLimitValue[] northLimits) {
//		this.northLimits = northLimits;
//	}
//	public AlarmLimitValue[] getNorthLimits() {
//		return northLimits;
//	}
//	
//	public void setSouthLimits(AlarmLimitValue[] southLimits) {
//		this.southLimits = southLimits;
//	}
//	public AlarmLimitValue[] getSouthLimits() {
//		return southLimits;
//	}	
//	
//	public DegreesLimitStore getAltLimits() {
//		return altLimits;
//	}
//
//	public void setAltLimits(DegreesLimitStore altLimits) {
//		this.altLimits = altLimits;
//	}
	
	
	public void setWorstLimits(AlarmLimitValue[] worstLimits) {
		this.worstLimits = worstLimits;
	}
	public AlarmLimitValue[] getWorstLimits() {
		return worstLimits;
	}	
	
	public void setBridgeLimits(AlarmLimitValue[] bridgeLimits) {
		this.bridgeLimits = bridgeLimits;
	}
	public AlarmLimitValue[] getBridgeLimits() {
		return bridgeLimits;
	}
	
	public byte getFlag() {
	  	byte flag = 0;
//	  	if (getNorthLimits()!=null)
//	  		flag+=FLAG_NORTH;
//	  	if (getSouthLimits()!=null)
//	  		flag+=FLAG_SOUTH;
//	  	if (getNorthLimits()!=null)
//	  		flag+=FLAG_NORTH;
	  	if (getWorstLimits()!=null)
	  		flag+=FLAG_WORST;
	  	
	  	if (getBridgeLimits()!=null)
	  		flag+=FLAG_BRIDGE;
	  	if (getLimitStore()!=null) 
	  		flag+=FLAG_WWS;
	  	return flag;
	}



	
	
}
