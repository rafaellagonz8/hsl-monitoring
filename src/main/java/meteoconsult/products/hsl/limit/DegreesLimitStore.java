package meteoconsult.products.hsl.limit;

import java.util.HashMap;

import meteoconsult.products.hsl.App.AlarmLimit;

/**
 * Stores the limits for one station
 * Limits are dependent of degrees
 */
public class DegreesLimitStore  {
	private static AlarmLimit[] parseOrder = new AlarmLimit[] { AlarmLimit.ALARM_220,AlarmLimit.ALARM_160, AlarmLimit.ALARM_80, AlarmLimit.ALARM_STOP };
	private HashMap<Integer,int[]> degreesMap = new HashMap<Integer,int[]>();

	
	public DegreesLimitStore() {
		
	}
	
	
	
	//parses the line from the format dd,limit1,limit2,limit3,limit4
	public void putLine(String line) {
		String [] parts = line.split(",");
		if (parts.length==(parseOrder.length+1)) {
			Integer dd = Integer.parseInt(parts[0]);
			int[] limits = new int[parseOrder.length];
			for (int i=0; i<parseOrder.length;i++) {
				limits[i] = Integer.parseInt(parts[i+1]); 
			} //for i
			degreesMap.put(dd, limits);
		} //if length
	}

	/**
	 * 
	 * @param dd
	 * @return
	 */
	public AlarmLimitValue[] getLimits(Integer dd) {
		AlarmLimitValue[] limVals = null;
		if (dd!=null) {
			int[] vals = degreesMap.get(dd);
			if (vals!=null) {
				limVals = new AlarmLimitValue[vals.length];
				for (int i = 0; i<vals.length;i++) {
					limVals[i] = new AlarmLimitValue(parseOrder[i],vals[i]);
				}
			} //vals not null
		}
		return limVals;
	}


}

