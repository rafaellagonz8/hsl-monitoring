package meteoconsult.products.hsl.limit;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;

import meteoconsult.database.tables.Table;
import meteoconsult.products.hsl.App;
import meteoconsult.products.hsl.App.AlarmLimit;

import org.apache.log4j.Logger;

import ChartDirector.XYChart;

public class LimitProvider {
	private Logger log = Logger.getLogger(LimitProvider.class.getName());
	/* cache the AlarmLimitValues, to prevent threading problems, synchronize this map otherwise it may grow over the defined size */
	private static Map<String,AlarmLimitValue> cacheMap= Collections.synchronizedMap(new LinkedHashMap<String,AlarmLimitValue>() {
		@Override
		protected boolean removeEldestEntry(Map.Entry<String,AlarmLimitValue> eldest) {
			return size() > 250;
		}
	});

	
	private final static int ALT_STATION = 1;

	private HashMap<Integer,StationLimits> allStationLimits = new HashMap<Integer,StationLimits>();

	//fixed sets
	/*  before 27 jan 2014
	public static AlarmLimitValue[] LIMVALS_NORTH = new AlarmLimitValue[] {
		new AlarmLimitValue(AlarmLimit.ALARM_220,43)
		,new AlarmLimitValue(AlarmLimit.ALARM_160,52)
		,new AlarmLimitValue(AlarmLimit.ALARM_80,60)
		,new AlarmLimitValue(AlarmLimit.ALARM_STOP,71) };

	public static AlarmLimitValue[] LIMVALS_SOUTH = new AlarmLimitValue[] {
		new AlarmLimitValue(AlarmLimit.ALARM_220,47)
		,new AlarmLimitValue(AlarmLimit.ALARM_160,58)
		,new AlarmLimitValue(AlarmLimit.ALARM_80,66)
		,new AlarmLimitValue(AlarmLimit.ALARM_STOP,74) };

	public static AlarmLimitValue[] LIMVALS_BRIDGE = new AlarmLimitValue[] {
		new AlarmLimitValue(AlarmLimit.ALARM_220,59)
		,new AlarmLimitValue(AlarmLimit.ALARM_160,65)
		,new AlarmLimitValue(AlarmLimit.ALARM_80,70)
		,new AlarmLimitValue(AlarmLimit.ALARM_STOP,76) };
		*/
	//after 27 jan 2014
	public static AlarmLimitValue[] LIMVALS_NORTH = new AlarmLimitValue[] {
		new AlarmLimitValue(AlarmLimit.ALARM_220,54)
		//,new AlarmLimitValue(AlarmLimit.ALARM_160,52)
		//,new AlarmLimitValue(AlarmLimit.ALARM_80,60)
		,new AlarmLimitValue(AlarmLimit.ALARM_STOP,62) };

	public static AlarmLimitValue[] LIMVALS_SOUTH = new AlarmLimitValue[] {
		new AlarmLimitValue(AlarmLimit.ALARM_220,54)
		//,new AlarmLimitValue(AlarmLimit.ALARM_160,58)
		//,new AlarmLimitValue(AlarmLimit.ALARM_80,66)
		,new AlarmLimitValue(AlarmLimit.ALARM_STOP,62) };

	public static AlarmLimitValue[] LIMVALS_BRIDGE = new AlarmLimitValue[] {
		new AlarmLimitValue(AlarmLimit.ALARM_220,54)
		,new AlarmLimitValue(AlarmLimit.ALARM_160,59)
		,new AlarmLimitValue(AlarmLimit.ALARM_80,64)
		,new AlarmLimitValue(AlarmLimit.ALARM_STOP,69) };
	
	public final static AlarmLimitValue LIMVAL_NORMAL = new AlarmLimitValue(AlarmLimit.NORMAL,0);

	
	
	/**
	 * Loads the limits for each station.
	 * Must be called once at initialization
	 */
	public void	init() {
		log.info("initialising limitProvider");
		//from first prio to second
		//first from File
		loadAlarmLimitsFromFile();
		/*
		//bridge
		for (int stat : App.BRIDGE_STATS) {
			StationLimits sl = getStationsLimit(stat);
			//only add bridge limits if there are no wws limits
			if (sl.getWwsLimits()==null) {
				getStationsLimit(stat).setBridgeLimits(	LIMVALS_BRIDGE );
			}
		}

		//south
		for (int stat : App.SOUTH_STATS) {
			//getStationsLimit(stat).setSouthLimits(	LIMVALS_SOUTH );
			//getStationsLimit(stat).setSouthLimits(	LIMVALS_WORST );
		} //south

		//NORTH
		for (int stat : App.NORTH_STATS) {
			//getStationsLimit(stat).setNorthLimits(	LIMVALS_NORTH );
			//getStationsLimit(stat).setNorthLimits(	LIMVALS_WORST );
		} //south
		*/
	}

	/**
	 *Is called from loadAlarmLimits to load and parse the specific stationlimits from resourcefiles
	 * Must be called once
	 */
	private void loadAlarmLimitsFromFile() {	
		//Obtains the folder of /src/resources
		URL dirURL = this.getClass().getClassLoader().getResource("");
		if (dirURL != null && dirURL.getProtocol().equals("file")) {
			try {
				FilenameFilter textFilter = new FilenameFilter() {
					public boolean accept(File dir, String name) {
						String lowercaseName = name.toLowerCase();
						if (lowercaseName.endsWith(".txt")) {
							return true;
						} else {
							return false;
						}
					}
				};
				
				String[] resFiles = new File(dirURL.toURI()).list(textFilter);
				for (String resFile: resFiles) {
					//parse station number, take the part between _ and .  (example tabel_1606361.txt);				
					String sLabel = resFile.substring(resFile.indexOf("_")+1,resFile.lastIndexOf("."));
					int station;
					if (sLabel!=null) {
						//add alt table
						if ("alt".equals(sLabel)) {
							station = ALT_STATION;
						}
						//all 'normal' station tables
						else {
							station = Integer.parseInt(sLabel);
						}
						String longFileName = URLDecoder.decode(dirURL.getPath(),"UTF-8")+"/"+resFile;
						File f = new File(longFileName);
						DegreesLimitStore store = new DegreesLimitStore();
						BufferedReader br = null;
						try {
							br = new BufferedReader(new FileReader(f));
							String oldString ="";
							String line;
							while ( (line=br.readLine())!=null) {
								oldString=oldString+line+"\n";
								//parse line
								store.putLine(line);
							} //while

							//store station >>> limits
							getStationsLimit(station).setLimitStore(store);

						} //try
						catch (FileNotFoundException e) {
							e.printStackTrace();
						}
						catch (IOException e) {
							e.printStackTrace();
						}
						finally {
							if (br!=null) {
								try { br.close();	}
								catch (IOException e) {	e.printStackTrace(); }
							}
						} //finally			
					}
				} //for resFile
			} //try
			catch (URISyntaxException e) {
				e.printStackTrace();
			}
			catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
		} //if file
	}

	/**
	 *
	 * @param station
	 * @return
	 */
	public StationLimits getStationsLimit(Integer station) {
		if (App.ALTSTATIONS_SET.contains(station)) {
			station = ALT_STATION;
		}	
		StationLimits sl = allStationLimits.get(station);
		if (sl==null) {
			sl = new StationLimits();
			allStationLimits.put(station,sl);
		}
		return sl;
	}



	/**
	 * Returns the worst limit for this station depending on f and for wws stations also dd
	 * @param stationId
	 * @param f
	 * @param dd
	 * @return
	 */
	public AlarmLimitValue getWorstLimit(int stationId, Number f, Number dd) {
		AlarmLimitValue worst = null;
		if (f==null) {
			return null;
		}
		else {	
			int ff = f.intValue();
			StationLimits sl = getStationsLimit(stationId);
			if (sl!=null) {
				//first try from cache
				String key = stationId+"_"+f;
				if (sl.getLimitStore()!=null) {
					key+="_"+dd; //only use dd in the key for wws stations
				}
				worst = cacheMap.get(key);
				if (worst!=null) {
					return worst;
				}
				else {
					//wws
					AlarmLimitValue worstWws = null;
					DegreesLimitStore wws = sl.getLimitStore();
					if (wws!=null && dd!=null) {
						AlarmLimitValue[] wwsLimits = wws.getLimits(dd.intValue());
						worstWws = getCurrentLimit(ff, wwsLimits);
					}
//					//north
//					AlarmLimitValue[] northLimits = sl.getNorthLimits();
//					AlarmLimitValue worstNorth = getCurrentLimit(ff, northLimits);
//					//south
//					AlarmLimitValue[] southLimits = sl.getSouthLimits();
//					AlarmLimitValue worstSouth = getCurrentLimit(ff, southLimits);
					//altCase			
//					AlarmLimitValue worstAlt = null;
//					DegreesLimitStore alt = sl.getAltLimits();
//					if (wws!=null && dd!=null) {
//						AlarmLimitValue[] altLimits = alt.getLimits(dd.intValue());
//						worstAlt = getCurrentLimit(ff, altLimits);
//					}
					
					
					//bridge
					AlarmLimitValue[] bridgeLimits = sl.getBridgeLimits();
					AlarmLimitValue worstBridge = getCurrentLimit(ff, bridgeLimits);

					HashSet<AlarmLimitValue> allWorst = new HashSet<AlarmLimitValue>();
					if (worstWws!=null) allWorst.add(worstWws);
//					if (worstNorth!=null) allWorst.add(worstNorth);
//					if (worstSouth!=null) allWorst.add(worstSouth);
//					if (worstAlt!=null) allWorst.add(worstAlt);
					if (worstBridge!=null) allWorst.add(worstBridge);
					if (allWorst.size()>0) {
						AlarmLimitValue[] allWorstArray = new AlarmLimitValue[allWorst.size()];
						allWorst.toArray(allWorstArray);
						worst = getCurrentLimit(ff,allWorstArray);
					}
					//finish off
					if (worst==null) {
						worst = LIMVAL_NORMAL;
					}
					cacheMap.put(key, worst);
					
				} //else (=no map entry yet
			} //sl not null
		} //else (f not null)
		return worst;
	}
	
	
//	
//	/**
//	 * Returns the worst limit for this station depending on f and for wws stations also dd
//	 * @param stationId
//	 * @param f
//	 * @param dd
//	 * @return
//	 */
//	public void getAllLimits(int stationId, Number dd) {
//			StationLimits sl = getStationsLimit(stationId);
//			if (sl!=null) {
//
//					//wws
//					AlarmLimitValue worstWws = null;
//					DegreesLimitStore wws = sl.getWwsLimits();
//					if (wws!=null && dd!=null) {
//						AlarmLimitValue[] wwsLimits = wws.getLimits(dd.intValue());
//					}
//					//north
//					AlarmLimitValue[] northLimits = sl.getNorthLimits();
//
//					//south
//					AlarmLimitValue[] southLimits = sl.getSouthLimits();
//
//					//bridge
//					AlarmLimitValue[] bridgeLimits = sl.getBridgeLimits();
//
//					
//					
//			} //sl not null
//	}

	/**
	 * Finds the worst limit from the limits array
	 * @param f
	 * @param limits
	 * @return
	 */
	private AlarmLimitValue getCurrentLimit(int f, AlarmLimitValue[] limits  ) {
		if (limits==null)
			return null;

		AlarmLimitValue validLimit = null;
		for (AlarmLimitValue limit: limits) {
			if (f>=limit.getValue()) {
				if (validLimit==null) {
					validLimit = limit;
				}
				else if (validLimit.getValue()<=limit.getValue() ) {
					validLimit = limit;
				}
			}
		} //for limit
		return validLimit;
	}


} //end class LimitProvider
