package meteoconsult.products.hsl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;
import java.util.Vector;

import meteoconsult.database.connector.PoolException;
import meteoconsult.database.connector.PooledJDBCConnection;
import meteoconsult.database.tables.Table;
import meteoconsult.database.tables.meteo.Knmi_obs10_rtm;
import meteoconsult.database.tables.meteo.Landstation;
import meteoconsult.database.tables.meteo.Mg_forecast;
import meteoconsult.database.tables.meteo.Plc_obs_rtm;
import meteoconsult.database.tables.meteo.key.FcstKeyable;
import meteoconsult.database.tables.meteo.key.FcstKeys;
import meteoconsult.database.tables.rbsadmin.Alarm;
import meteoconsult.database.tables.rbsadmin.AlarmMod;
import meteoconsult.database.tables.rbsadmin.AlarmType;
import meteoconsult.database.tables.rbsadmin.HslWarningState;
import meteoconsult.lib.common.table.CssTableModel;
import meteoconsult.lib.common.table.cell.CssCell;
import meteoconsult.lib.common.table.cell.DateCell;
import meteoconsult.lib.common.table.cell.HeaderCell;
import meteoconsult.lib.common.table.cell.SpannableCssCell;
import meteoconsult.lib.common.table.cell.TagAddition;
import meteoconsult.products.hsl.App.AlarmLimit;
import meteoconsult.products.hsl.limit.AlarmLimitValue;
import meteoconsult.products.hsl.limit.DegreesLimitStore;
import meteoconsult.products.hsl.limit.StationLimits;
import meteoconsult.products.hsl.table.StateCell;
import meteoconsult.products.hsl.table.WindCell;
import meteoconsult.products.hsl.table.WindDirCell;
import meteoconsult.util.DatePlusString;
import meteoconsult.util.DateTime;
import meteoconsult.util.Display;
import meteoconsult.util.StringUtil;

import static meteoconsult.products.hsl.App.*;

/**
 * Is a central point for data. At this moment there is one HslDataProvider per session. 
 */
public class HslDataProvider {

	//fill id arrays 
	private final static int[] ONE_MIN_STAT_IDS = { ID_HSL_GSM_MAST, ID_HSL_HELLEGATSPLEIN, ID_HSL_MAASDAM,ID_HSL_MARK,
		ID_HSL_MOERDIJKBRUG, ID_HSL_BRIDGE,	ID_HSL_KLUNDERT , ID_HSL_HOLL_SLUIS };
	private final static int[] TEN_MIN_STAT_IDS = { ID_VOORSCHOTEN, ID_AMS, ID_RTM, ID_GLZ, 
		ID_IJMUIDEN, ID_ZANDVOORT, ID_VLISSINGEN, ID_OOSTERSCHELDE, ID_VLAKTE_VAN_RAAN, 
		ID_HANSWEERT, ID_WESTDORPE_AWS, ID_LICHTEILAND_GOEREE, ID_EUROPLATFORM, 
		ID_HOEK_VAN_HOLLAND, ID_MAROLLEGAT, ID_TER_HEIJDE, 
		ID_WOENSDRECHT, ID_ROTTERDAM_GEULHAVEN
		//new
		,ID_ASSENDELFT,ID_MUIDEN,ID_NIEUW_VENNEP,ID_IJMOND,ID_STAVENISSE,ID_CADZAND,ID_HOOFDPLAAT
		// new 27.09.2018
		,ID_WILHELMINADORP
	};	

	//private final static String ALARM_FASE_PATH = "//fs01/Temp/hsl/HSLBAlarmfase.txt";
	private final static String ALARM_FASE_PATH = "/var/www/Data/resources/hsl/HSLBAlarmfase.txt";


	private final static long OBS_GRAPH_HOURS = 6 * DateTime.HOUR;
	private final static long FCST_GRAPH_HOURS = 24 * DateTime.HOUR;



	//private final static float BLOCK_SIZE = 15.0f; // number of rows per table block 


	//private Vector<HslData> latestDatas = new Vector<HslData>();
	private Vector<HslData> oneMinDatas = new Vector<HslData>(); //used for actual 1 min obs
	private Vector<HslData> tenMinDatas = new Vector<HslData>(); //used for actual 10 min obs

	private HashMap<Integer,HslData> refMap = new HashMap<Integer,HslData>(); 
	private Calendar cal = Calendar.getInstance();
	private TimeZone timeZone;
	private SimpleDateFormat sdfMinute = new SimpleDateFormat("EEE HH:mm");
	private SimpleDateFormat sdfAlarmFase = new SimpleDateFormat("yyyyMMddHHmmss");

	private HslData actualBridgeF;
	private HslData maxBridgeFf;
	private HslData maxBridgeFx;

	private HslData max1MinFf;
	private HslData max1MinFx;

	private HslData max10MinFf;
	private HslData max10MinFx;

	private HslData selectedHslStation;

	private Plc_obs_rtm bridgeGraphData;
	private Mg_forecast bridgeFcstData;
	private Table varGraphData;

	private CssTableModel varTable;
	private CssTableModel legendTable;

	private Date refreshTime;
	private Date lastObsStartDate;
	private Date lastObsEndDate;

	private Date lastFcstStartDate;

	private CssTableModel alarmTableNorth;
	private CssTableModel alarmTableSouth;
	private CssTableModel alarmTableSummary;
	private HashMap<Integer,Integer> totalAlarmCount; //counts the unique alarms
	private HashMap<Integer,Integer> northAlarmCount;
	private HashMap<Integer,Integer> southAlarmCount;

	private DatePlusString lastAlarmFase;
	private ArrayList<HslWarningState> warningState;
	private Boolean isWarned;
	private String isWarnedMessage="";


	/**
	 * The central location for changes in data
	 *
	 */
	public HslDataProvider() {
		initiate(); 
		log.info("new HslDataProvider created");
	};


	/**
	 * called by constructor
	 */
	private void initiate() {
		sdfAlarmFase.setTimeZone(DateTime.UTC);

		initHslDatas(ONE_MIN_STAT_IDS, oneMinDatas, HslData.ONE_MIN_STATION);
		initHslDatas(TEN_MIN_STAT_IDS, tenMinDatas, HslData.TEN_MIN_STATION);
		actualBridgeF = refMap.get(ID_HSL_BRIDGE);
		selectedHslStation=refMap.get(ID_HSL_GSM_MAST);
	}

	/**
	 * Initializes the HslData's that contain latest obs for the stations.
	 * @param hslStations
	 * @param datas
	 */
	private void initHslDatas(int[] hslStations, Vector<HslData> datas, int type) {	
		//load corresponding landstations
		Landstation[] stations = Landstation.getLandstations(hslStations);

		for (int i=0;i< hslStations.length;i++ ) {
			int hslStationId = hslStations[i];
			HslData data = new HslData(hslStationId);
			Landstation station =stations[i];
			if (station!=null) {
				data.setName(station.getLandstat_name_wmo());
				data.setOrgName(station.getOrg_name());
				data.setLat( station.getDecimalLat() );
				data.setLon( station.getDecimalLon() );		
				data.setType(type);
				datas.add(data);
				refMap.put(data.getStatId(),data);
			} //station not null
			else {
				log.warning("landstation "+hslStationId+" does not exsist");
			}
		} //for hslStationsIds
	}

	/**
	 * Returns the HSLData's filled with the latest available data's
	 * @return
	 */
	public  Vector<HslData> get10MinDatas() {
		return tenMinDatas;
	}

	/**
	 * Returns the HSLData's filled with the latest available data's
	 * @return
	 */
	public  Vector<HslData> get1MinDatas() {
		return oneMinDatas;
	}

	/**
	 * Checks the latest dtg for the given stations and fills with new data if needed
	 * @param obsStartDate  the obsEndDate minus the choosen interval
	 * @param obsEndDate	the obsEndDate
	 * @return
	 */
	public boolean refresh(Date obsStartDate, Date obsEndDate) {
		boolean isRefreshed=false;
		if ( (lastObsStartDate==null)||(lastObsEndDate==null) 
				|| (!lastObsStartDate.equals(obsStartDate)) || (!lastObsEndDate.equals(obsEndDate))  ) {
			refreshTime = new Date();

			//get the most recent dtg's between start and end date
			PooledJDBCConnection mydb = new PooledJDBCConnection(meteoconsult.database.PackageStatics.DB_METEO);

			String sStartDate = DateTime.SDF_YEAR_TO_MIN.format(obsStartDate);
			String sEndDate = DateTime.SDF_YEAR_TO_MIN.format(obsEndDate);

			//log.info("refresh "+sStartDate+" - "+sEndDate);
			try {
				//load obs for graph and table and actual bridge obs
				load1MinBridgeObs(obsStartDate,obsEndDate);

				//load 1 min obs for the google map
				loadObs1Min(mydb,sStartDate,sEndDate);

				//if (obsStartDate.getTime()-latestObsStartTime) //TODO: look if not longer then x minutes 
				loadObs10Min(mydb,sStartDate,sEndDate);

				//load the max wind
				maxBridgeFf = getMaxWind1min(mydb, ID_HSL_BRIDGE, sStartDate, sEndDate, "ff");
				maxBridgeFx = getMaxWind1min(mydb, ID_HSL_BRIDGE, sStartDate, sEndDate, "fx");

				max1MinFf = getMaxWind1min(mydb, ONE_MIN_STAT_IDS, sStartDate, sEndDate, "ff");
				max1MinFx = getMaxWind1min(mydb, ONE_MIN_STAT_IDS, sStartDate, sEndDate, "fx");

				max10MinFf    = getMaxWind10min(mydb, TEN_MIN_STAT_IDS, sStartDate, sEndDate, "ff");
				max10MinFx    = getMaxWind10min(mydb, TEN_MIN_STAT_IDS, sStartDate, sEndDate, "ffg_10");

				//load the latest fcst if needed
				cal.setTime(obsEndDate);
				cal.set(Calendar.MINUTE,0);
				cal.set(Calendar.SECOND,0);
				cal.set(Calendar.MILLISECOND,0);
				if ( (lastFcstStartDate==null) || (cal.getTime().after(lastFcstStartDate)) ) {
					loadFcstBridgeData(cal.getTime());
				}

				//load obs for graph and table for variable station
				refreshVarData(selectedHslStation, obsStartDate, obsEndDate);

				readLastAlarmFase();
				loadWarningState(obsStartDate, obsEndDate);
				//TODO: restore
				//				Date obsStartFlut = new Date("4/21/2011 11:00:00 GMT");
				//				Date obsEndFlut = new Date("4/21/2011 12:25:00 GMT");
				//				refreshAlarmTables(obsStartFlut, obsEndFlut);
				refreshAlarmTables(obsStartDate, obsEndDate);

				refreshIsWarned();

				//finish
				lastObsStartDate = obsStartDate;
				lastObsEndDate = obsEndDate;
				isRefreshed=true;
			} //try
			catch (SQLException ex) { log.severe("SQLException, msg: " + ex.getMessage()); }
			finally { mydb.closeReq();}

		} //if different start and enddate

		return isRefreshed;
	}

	private void refreshVarData(HslData selStation, Date obsStartDate, Date obsEndDate) {
		if (selStation==null) {
			log.severe("unknown station selected, not available in refMap");
			return;
		}

		if (selStation.getType()==HslData.ONE_MIN_STATION) {
			load1MinVarObs(selStation,obsStartDate, obsEndDate);
		}
		else {
			load10MinVarObs(selStation,obsStartDate, obsEndDate);
		}
	}

	/**
	 * Returns the time the refresh is initiated
	 * @return
	 */
	public Date getRefreshTime() {
		return refreshTime;
	}

	public void setSelectedStation(int selectedStation) {
		if (selectedHslStation==null) {
			selectedHslStation = refMap.get(selectedStation);
			refreshVarData(selectedHslStation , lastObsStartDate, lastObsEndDate);
		}
		else if (selectedStation!=selectedHslStation.getStatId()) {
			selectedHslStation = refMap.get(selectedStation);	
			refreshVarData(selectedHslStation , lastObsStartDate, lastObsEndDate);
		}
		//log.info("selectedHslStation nu: "+(selectedHslStation==null ? "null" : selectedHslStation.getName()) );
	}

	public HslData getSelectedHslData() {
		return selectedHslStation;
	}

	public HslData getHslData(int stationId) {
		return refMap.get(stationId);
	}



//	private void artificialyRaiseWindForTesting(HslData hslData, int plus) {
//		log.warning("!!!! artificially raising windspeed. ONLY FOR TEST. REMOVE THIS!!!");
//		if (hslData!=null) {
//			if (hslData.getFf()!=null) {
//				hslData.setFf(hslData.getFf()+plus);
//			}
//			if (hslData.getFx()!=null) {
//				hslData.setFx(hslData.getFx()+plus);
//			}
//		}
//	}


	/**
	 * Queries the max ff or fx for the latest occurence between start and end date
	 * in 1 minute data in plc_obs_rtm obs
	 * @param mydb
	 * @param statIds
	 * @param sStartDate
	 * @param sEndDate
	 * @param element   either fx or ff
	 * @return
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 * @throws PoolException
	 */
	private HslData getMaxWind1min(PooledJDBCConnection mydb, int statId,String sStartDate, String sEndDate, String element) throws SQLException {
		return getMaxWind1min(mydb,  new int[] {statId}, sStartDate, sEndDate, element);
	}


	/**
	 * Queries the max ff or fx for the latest occurence between start and end date
	 * in 1 minute data in plc_obs_rtm obs
	 * @param mydb
	 * @param statIds
	 * @param sStartDate
	 * @param sEndDate
	 * @param element   either fx or ff
	 * @return
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 * @throws PoolException
	 */
	private HslData getMaxWind1min(PooledJDBCConnection mydb, int[] statIds,String sStartDate, String sEndDate, String element) throws SQLException {
		HslData maxData = null;

		String stats;
		if (statIds.length==0) 
			return null;
		else if (statIds.length==1) {
			stats= " = "+statIds[0];
		}
		else {
			stats= " in ("+StringUtil.implode(statIds,",")+")";
		}

		//load the max wind
		String sql="SELECT first 1  stat_id, dtg, ff, fx, dd FROM plc_obs_rtm "
				+" WHERE dtg>'"+sStartDate+"'"
				+" 	 AND dtg<='"+sEndDate+"'"
				+" 	 AND stat_id "+stats
				+" 	 AND "+element+" = ("
				+" 	    SELECT max ("+element+") FROM plc_obs_rtm " 
				+" 	    WHERE dtg>'"+sStartDate+"'"
				+" 	    AND dtg<='"+sEndDate+"'"
				+" 	    AND stat_id  "+stats+" )"
				+" 	ORDER by dtg desc";	
		mydb.sendRequest(sql);
		ResultSet rs = mydb.getRs();
		if (rs.next()) {
			maxData = new HslData(rs.getInt("stat_id"));
			maxData.setDtg(rs.getDate("dtg"));
			maxData.setFf(rs.getInt("ff"));
			maxData.setDd(rs.getInt("dd"));
			maxData.setFx(rs.getInt("fx"));
			HslData refData = refMap.get(maxData.getStatId());
			if (refData!=null) {
				maxData.setName(refData.getName());
				maxData.setOrgName(refData.getOrgName());
			}


		} //if rs.next()
		rs.close();
		return maxData;
	}




	/**
	 * Queries the max ff or fx for the latest occurence between start and end date
	 *  in 10 minute data in knmi_obs10_rtm
	 * @param mydb
	 * @param statIds
	 * @param sStartDate
	 * @param sEndDate
	 * @param element   either fx or ff
	 * @return
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 * @throws PoolException
	 */
	private HslData getMaxWind10min(PooledJDBCConnection mydb, int[] statIds,String sStartDate, String sEndDate, String element) throws SQLException {
		String stats;
		if (statIds.length==0) 
			return null;
		else {
			stats= "("+StringUtil.implode(statIds,",")+")";
		}
		HslData maxData = null;
		//load the max wind
		String sql="SELECT first 1  stat_id, dtg, ff, ffg_10, dd FROM knmi_obs10_rtm "
				+" WHERE dtg>'"+sStartDate+"'"
				+" 	 AND dtg<='"+sEndDate+"'"
				+" 	 AND stat_id in " +stats
				+" 	 AND "+element+" = ("
				+" 	    SELECT max ("+element+") FROM knmi_obs10_rtm " 
				+" 	    WHERE dtg>'"+sStartDate+"'"
				+" 	    AND dtg<='"+sEndDate+"'"
				+" 	    AND stat_id in "+stats+" )"
				+" 	ORDER by dtg desc";	
		mydb.sendRequest(sql);
		ResultSet rs = mydb.getRs();
		if (rs.next()) {
			maxData = new HslData(rs.getInt("stat_id"));
			maxData.setDtg(rs.getDate("dtg"));
			maxData.setFf( Math.round(rs.getFloat("ff")) );
			maxData.setDd(rs.getInt("dd"));
			maxData.setFx( Math.round(rs.getFloat("ffg_10")) );
			HslData refData = refMap.get(maxData.getStatId());
			if (refData!=null) {
				maxData.setName(refData.getName());
				maxData.setOrgName(refData.getOrgName());
			}
			
			
		} //if rs.next()
		rs.close();
		return maxData;
	}

	public HslData getActualBridgeF() {
		return actualBridgeF;
	}


	public HslData getMaxBridgeFf() {
		return maxBridgeFf;
	}

	public HslData getMaxBridgeFx() {
		return maxBridgeFx;
	}

	public HslData getMax1MinFf() {
		return max1MinFf;
	}

	public HslData getMax1MinFx() {
		return max1MinFx;
	}


	public HslData getMax10MinFf() {
		return max10MinFf;
	}

	public HslData getMax10MinFx() {
		return max10MinFx;
	}

	//---- bridge obs and fcst ------------

	/**
	 * Always needed
	 *
	 */
	private void load1MinBridgeObs(Date obsStartDate, Date obsEndDate) {
		int stationId = ID_HSL_BRIDGE;

		//load the bridge graph data
		Date startGraphDate = new Date(obsEndDate.getTime()-OBS_GRAPH_HOURS);
		//if obStartDate longer back in history then use this
		if (obsStartDate.before(startGraphDate)) 	startGraphDate=obsStartDate;

		Plc_obs_rtm newTable = new Plc_obs_rtm();
		newTable.getObservations(stationId,startGraphDate, obsEndDate);

		//use the last entry with non-null Ff to fill the bridge hsl data
		actualBridgeF = refMap.get(ID_HSL_BRIDGE);
		if (actualBridgeF!=null) {
			newTable.afterLast();
			boolean goOn = true;
			while (goOn && newTable.previous()) {
				Number nFf = newTable.getNumber("ff");
				if (nFf!=null) {
					actualBridgeF.setFf(nFf.intValue());
					actualBridgeF.setDtg(newTable.getTimestamp("dtg"));
					Number nDd = newTable.getNumber("dd");
					actualBridgeF.setDd( (nDd==null) ? null: nDd.intValue() );
					Number nFx = newTable.getNumber("fx");
					actualBridgeF.setFx((nFx==null) ? null: nFx.intValue());
					goOn=false;
				}
			} //while
		} //bridgeHslData not null
		newTable.beforeFirst();

		bridgeGraphData = newTable;
	}

	public Plc_obs_rtm getBridgeGraphData() {
		return bridgeGraphData;
	}

	/**
	 * @param fcstStartDate this is a fcst, so the fcstStartDate is the obsEndDate
	 */
	private void loadFcstBridgeData(Date fcstStartDate) {
		Date endGraphDate = new Date(fcstStartDate.getTime()+FCST_GRAPH_HOURS);
		Mg_forecast fcstTable = new Mg_forecast();
		//bridgeFcstData.getLatestFcstFromNow(ID_HSL_BRIDGE, Mg_forecast.IDENTIFIER_MULTI, fcstStartDate, endGraphDate, null, null);
		fcstTable.getLatestFcstFromNow(ID_HSL_BRIDGE, fcstStartDate, endGraphDate);

		bridgeFcstData = fcstTable;
		lastFcstStartDate = fcstStartDate;
	}

	public Mg_forecast getBridgeFcstData() {
		return bridgeFcstData;
	}

	//---- variabele obs ------------

	private void load1MinVarObs(HslData station, Date obsStartDate, Date obsEndDate) {
		Date startGraphDate = new Date(obsEndDate.getTime()-OBS_GRAPH_HOURS);
		//if obStartDate longer back in history then use this
		if (obsStartDate.before(startGraphDate)) 	startGraphDate=obsStartDate;
		Plc_obs_rtm obsTable = new Plc_obs_rtm();
		obsTable.getObservations(station.getStatId(),startGraphDate, obsEndDate);
		varGraphData = obsTable;
		loadVarTableData(obsTable, station, obsStartDate, obsEndDate);
		createLegendTable(station,obsTable);
	}

	private void load10MinVarObs(HslData station, Date obsStartDate, Date obsEndDate) {
		Date startGraphDate = new Date(obsEndDate.getTime()-OBS_GRAPH_HOURS);
		//if obStartDate longer back in history then use this
		if (obsStartDate.before(startGraphDate)) 	startGraphDate=obsStartDate;
		Knmi_obs10_rtm obsTable = new Knmi_obs10_rtm();
		obsTable.getWindObservations(station.getStatId(),startGraphDate, obsEndDate);
		varGraphData = obsTable;
		loadVarTableData(obsTable, station, obsStartDate, obsEndDate);
		createLegendTable(station,obsTable);
	}

	/**
	 * Load the data for the var table
	 * @param dbTable
	 * @param station HslData, only for name and type, not for the current data!
	 * @param obsStartDate
	 * @param obsEndDate
	 */
	private void loadVarTableData(Table dbTable,HslData station, Date obsStartDate, Date obsEndDate) {
		if (dbTable instanceof FcstKeyable) {
			FcstKeys tableKeys = ((FcstKeyable)dbTable).getFcstKeys();
			//dbTable.fcs
			//copy the data for the var table from the last hour of the var dbTable (must be <= then GRAPH_HOURS
			varTable = new CssTableModel();
			varTable.setNrHeaderRows(1);
			int row=0;
			int col=0;	
			varTable.setValueAt(new HeaderCell(("("+station.getStatId()+") "+station.getName()).toUpperCase(),"name_col",4), row,col++);
			row++; col=0;
			varTable.setValueAt(new HeaderCell("dtg","dtg_col"), row,col++);
			varTable.setValueAt(new HeaderCell("dd","val_col"), row,col++);
			varTable.setValueAt(new HeaderCell("ff","val_col"), row,col++);
			varTable.setValueAt(new HeaderCell("fx","val_col"), row,col++);
			row++; col=0;
			cal.setTime(obsEndDate);
			Date end = obsStartDate;
			// if 10 min data then round down to 10
			if (station.getType()==HslData.TEN_MIN_STATION) {
				int roundedMinute = (cal.get(Calendar.MINUTE)/10) * 10;
				cal.set(Calendar.MINUTE,roundedMinute);
			}

			dbTable.afterLast();
			Date dtg = null;
			boolean dataAvailable = dbTable.previous();
			//get the first one
			if (dataAvailable) {
				dtg = dbTable.getTimestamp("dtg");

				//i want a list with any minute from curr obs till 1 hour back. I need to find the 
				//corresponding dbTable
				while ( cal.getTime().after(end) ) {
					Date dt = cal.getTime();
					//set the time column
					DateCell dtgCell = new DateCell(dt,sdfMinute);
					dtgCell.setCssClass("dtg_val");
					varTable.setValueAt(dtgCell, row,col++);
					//get a new record
					while (dataAvailable && dt.before(dtg) ) {
						dataAvailable = dbTable.previous();
						if (dataAvailable)
							dtg = dbTable.getTimestamp(tableKeys.DTG.dbKey);
					} //while dtg more recent then dt
					if (dtg.getTime()==dt.getTime() ) {
						Number dd = dbTable.getNumber(tableKeys.WIND_10M_DIRECTION.dbKey);
						varTable.setValueAt(new WindDirCell(dbTable.getObject(tableKeys.WIND_10M_DIRECTION)), row,col++);
						varTable.setValueAt(new WindCell(station.getStatId(), dbTable.getNumber(tableKeys.WIND_10M_KNOTS.dbKey), dd ), row,col++);
						varTable.setValueAt(new WindCell(station.getStatId(), dbTable.getNumber(tableKeys.GUSTS_10M_KNOTS.dbKey), dd ), row,col++);
					}	
					if (station.getType()==HslData.TEN_MIN_STATION) {
						cal.add(Calendar.MINUTE, -10);
					}
					else {
						cal.add(Calendar.MINUTE, -1);
					}
					row++; 
					col=0;
				} //while
			} //if dataAvailable
			dbTable.beforeFirst();

		} //if FcstKeyable
	}

	public String getSelectedStationName() {
		if (selectedHslStation != null) {
			return "(" + selectedHslStation.getStatId() + ") " + selectedHslStation.getName();
		}
		else
			return "- none -";
	}

	public String getBridgeStationName() {
		if (actualBridgeF != null) {
			return "(" + actualBridgeF.getStatId() + ") " + actualBridgeF.getName();
		}
		else
			return "- none -";
	}



	private void createLegendTable(HslData station,Table dbTable) {
		if (dbTable instanceof FcstKeyable) {
			FcstKeys tableKeys = ((FcstKeyable)dbTable).getFcstKeys();
			//get last record
			dbTable.afterLast();
			boolean dataAvailable = dbTable.previous();
			if (dataAvailable) {
				int statId = station.getStatId();
				//copy the data for the var table from the last hour of the var dbTable (must be <= then GRAPH_HOURS
				legendTable = new CssTableModel();
				legendTable.setNrHeaderRows(1);
				int row=0;
				int col=0;

				legendTable.setValueAt(new SpannableCssCell(("legend "+station.getName()),"limit_header",1,2), row,col++);
				row++; col=0;
				legendTable.setValueAt(new HeaderCell("dtg","dtg_col"), row,col++);
				Date dtg = dbTable.getTimestamp(tableKeys.DTG.dbKey);
				legendTable.setValueAt( new DateCell(dtg,sdfMinute), row,col++);
				row++; col=0;
				legendTable.setValueAt(new HeaderCell("dd","val_col"), row,col++);
				Number nDd = dbTable.getNumber(tableKeys.WIND_10M_DIRECTION.dbKey);
				Integer dd = (nDd==null) ? null : nDd.intValue();
				legendTable.setValueAt(new WindDirCell(dd), row,col++);
				//				Number ff = dbTable.getNumber(tableKeys.WIND_10M_KNOTS.dbKey);
				//				Number fx = dbTable.getNumber(tableKeys.GUSTS_10M_KNOTS.dbKey);
				//				row++; col=0;
				//				legendTable.setValueAt(new HeaderCell("ff","val_col"), row,col++);
				//				legendTable.setValueAt(new WindCell(statId ,ff, dd), row,col++);
				//				row++; col=0;
				//				legendTable.setValueAt(new HeaderCell("fx","val_col"), row,col++);
				//				legendTable.setValueAt(new WindCell(statId ,fx, dd ), row,col++);

				row=row+1; col=0;

				//AlarmLimitValue[] limVals = App.getLimitProvider().getLimits(statId, dd);

				StationLimits sl = App.getLimitProvider().getStationsLimit(statId);
				if (sl!=null) {
					//wws
					DegreesLimitStore limitStore = sl.getLimitStore();
					if (limitStore!=null && dd!=null) {
						col=0;
						AlarmLimitValue[] storedLimits = limitStore.getLimits(dd.intValue());
						if (storedLimits!=null) {
							String source = "wws";
							if (App.ALTSTATIONS_SET.contains(statId)) {
								source = "alt station";
							}
							legendTable.setValueAt(new SpannableCssCell(source,"limit_header",1,2), row++,col);
							for (AlarmLimitValue wwsLimit : storedLimits) {
								legendTable.setValueAt(new LegendCell(wwsLimit.getLimit(),"limit_legend"), row,col);
								legendTable.setValueAt(new CssCell(wwsLimit.getValue()), row,col+1);
								row++;
							} //for wwsLimit
						}
					} //wws
					//					DegreesLimitStore alt = sl.getAltLimits();
					//					if (alt!=null && dd!=null) {
					//						col=0;
					//						AlarmLimitValue[] altLimits = alt.getLimits(dd.intValue());
					//						if (altLimits!=null) {
					//							legendTable.setValueAt(new SpannableCssCell("alt","limit_header",1,2), row++,col);
					//							for (AlarmLimitValue altLimit : altLimits) {
					//								legendTable.setValueAt(new LegendCell(altLimit.getLimit(),"limit_legend"), row,col);
					//								legendTable.setValueAt(new CssCell(altLimit.getValue()), row,col+1);
					//								row++;
					//							} //for altLimit
					//						}
					//					} //alt

					//					//north
					//					AlarmLimitValue[] northLimits = sl.getNorthLimits();
					//					if (northLimits!=null) {
					//						legendTable.setValueAt(new SpannableCssCell("north","limit_header",1,2), row++,col);
					//						for (AlarmLimitValue limit : northLimits) {
					//							legendTable.setValueAt(new LegendCell(limit.getLimit(),"limit_legend"), row,col);
					//							legendTable.setValueAt(new CssCell(limit.getValue()), row,col+1);
					//							row++;
					//						} //for northLimit
					//					} //north
					//
					//					//south
					//					AlarmLimitValue[] southLimits = sl.getSouthLimits();
					//					if (southLimits!=null) {
					//						legendTable.setValueAt(new SpannableCssCell("south","limit_header",1,2), row++,col);
					//						for (AlarmLimitValue limit : southLimits) {
					//							legendTable.setValueAt(new LegendCell(limit.getLimit(),"limit_legend"), row,col);
					//							legendTable.setValueAt(new CssCell(limit.getValue()), row,col+1);
					//							row++;
					//						} //for southLimit
					//					} //south

					//bridge
					AlarmLimitValue[] bridgeLimits = sl.getBridgeLimits();	
					if (bridgeLimits!=null) {
						legendTable.setValueAt(new SpannableCssCell("bridge","limit_header",1,2), row++,col);
						for (AlarmLimitValue limit : bridgeLimits) {
							legendTable.setValueAt(new LegendCell(limit.getLimit(),"limit_legend"), row,col);
							legendTable.setValueAt(new CssCell(limit.getValue()), row,col+1);
							row++;
						} //for bridgeLimit
					} //bridge
				} //sl not null

			} //data available
			dbTable.beforeFirst();
		} //if FcstKeyable
	}

	class LegendCell extends CssCell implements TagAddition {
		Integer colorCode = null;

		public LegendCell(AlarmLimit limit,String cssClass) {
			if (limit!=null) {
				super.setValue(limit.kmh());
				super.setCssClass(cssClass);

				colorCode = limit.colorCode();
			}
		}

		public String getTagAddition() {
			return "style='background-color:#"+Integer.toHexString(colorCode)+"'";
		}

	}


	/**
	 * Returns the value for this limit
	 * @param limit
	 * @return
	 */
	public Integer getLimitValue(AlarmLimit limit, AlarmLimitValue[] limVals) {
		Integer result = null;
		for (AlarmLimitValue limVal: limVals) {
			if (limVal.getLimit()==limit) {
				result = limVal.getValue();
				break;
			}
		}
		return result;
	}

	public Table getVarGraphData() {
		return varGraphData;
	}

	public CssTableModel getVarTableMdl() {
		return varTable;
	}

	public CssTableModel getLegendTableMdl() {
		return legendTable;
	}


	//----  obs of other stations------------


	/**
	 * Loads the latest obs from 1 min data for the other obs
	 * @param mydb
	 * @param sStartDate
	 * @param sEndDate
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 * @throws PoolException
	 */
	private void loadObs1Min(PooledJDBCConnection mydb, String sStartDate, String sEndDate) throws SQLException {
		//load the latest hsl data's
		String psql = "SELECT dtg, dd, ff, fx FROM plc_obs_rtm"
				+ " WHERE stat_id = ? "
				+ " AND dtg = ( SELECT max(dtg) FROM plc_obs_rtm "
				+ "   WHERE dtg>'"+sStartDate+"'"      
				+ "   AND dtg<='"+sEndDate+"'"
				+ "   AND (ff is not null or fx is not null)"
				+ "   AND stat_id = ? )";
		PreparedStatement pstmt = mydb.prepareStatement(psql);
		for (HslData hslData: oneMinDatas) {
			if (hslData!=null) {
				pstmt.setInt(1,hslData.getStatId());
				pstmt.setInt(2,hslData.getStatId());
				ResultSet rs = pstmt.executeQuery();
				if (rs.next()) {
					hslData.setDtg(rs.getDate("dtg"));
					hslData.setFf(rs.getInt("ff"));
					hslData.setDd(rs.getInt("dd"));
					hslData.setFx(rs.getInt("fx"));				
				} //if rsStation.next()
				else {
					hslData.setFf(null);
					hslData.setDd(null);
					hslData.setFx(null);
				}

				rs.close();
			} //hslData not null
		} //for hslData
	}


	/**
	 * Loads the latest obs from 10 min data for the other obs
	 * @param mydb
	 * @param sStartDate
	 * @param sEndDate
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 * @throws PoolException
	 */
	private void loadObs10Min(PooledJDBCConnection mydb, String sStartDate, String sEndDate) {
		try {
			//load the latest hsl data's
			String psql = "SELECT dtg, dd, ff, ffg_10 FROM knmi_obs10_rtm"
					+ " WHERE stat_id = ? "
					+ " AND dtg = ( SELECT max(dtg) FROM knmi_obs10_rtm"
					+ "   WHERE dtg>'"+sStartDate+"'"      
					+ "   AND dtg<='"+sEndDate+"'"
					+ "   AND (ff is not null or ffg_10 is not null)"
					+ "   AND stat_id = ? )";

			PreparedStatement pstmt = mydb.prepareStatement(psql);
			for (HslData hslData: tenMinDatas) {
				if (hslData!=null) {
					pstmt.setInt(1,hslData.getStatId());
					pstmt.setInt(2,hslData.getStatId());
					ResultSet rs = pstmt.executeQuery();	   	
					if (rs.next()) {
						hslData.setDtg(rs.getDate("dtg"));
						hslData.setFf(Math.round(rs.getFloat("ff")) );
						Object oDd = rs.getObject("dd");
						hslData.setDd( (oDd==null) ? null : ( (Number)oDd).intValue() );
						hslData.setFx(Math.round(rs.getFloat("ffg_10")) );
					} //if rsStation.next()
					else {
						hslData.setFf(null);
						hslData.setDd(null);
						hslData.setFx(null);
					}
					
					rs.close();
				} //hslData not null
			} //for hslData
		} //try 
		catch (SQLException ex) { 
			log.severe("SQLException in loadObs10Min("+sStartDate+","+ sEndDate+"), msg: " + ex.getMessage()); 
		}
	}

	//---- alarm ------------
	/**
	 * 
	 *
	 */
	public void refreshAlarmTables(Date obsStartDate, Date obsEndDate) {
		alarmTableNorth = new CssTableModel();
		alarmTableSouth = new CssTableModel();
		alarmTableSummary = new CssTableModel();
		HashSet<AlarmType> alarmTypes = new HashSet<AlarmType>();
		alarmTypes.add(AlarmType.getInstance(AlarmType.HSL));

		totalAlarmCount = new HashMap<Integer,Integer>();
		northAlarmCount = new HashMap<Integer,Integer>();
		southAlarmCount = new HashMap<Integer,Integer>();
		List<Alarm> alarms = Alarm.getAlarms(obsStartDate, obsEndDate, alarmTypes) ; 
		/*-----------TEST ALARM--------------*/
		/*
		Alarm flut = new Alarm();
		Date now = new Date();
		flut.setDtg(DateTime.addHours(now,-1));
		flut.setLandstatId(6330);
		flut.setAlarmType(AlarmType.HSL);
		flut.setMessage("flut alarm");
		AlarmMod flutMod = new AlarmMod();
		flutMod.setAlarm(flut);
		flutMod.setAuthor("test");
		flutMod.setAlarmState(AlarmMod.STATE_OPEN);
		flutMod.setDtg(now);
		flutMod.setComment("modificatie gemaakt voor test");
		LinkedHashSet<AlarmMod> modSet = new LinkedHashSet<AlarmMod>();
		modSet.add(flutMod);
		flut.setAlarmMods(modSet);
		alarms.add(flut);
		 */


		log.info("in refreshAlarmTables, sz: "+alarms.size());
		if (alarms.size()>0) {

			for (Alarm alarm : alarms) {
				String orgName = "";
				String stationName = "";
				Landstation station = alarm.getLandstation();
				if (station!=null) {
					if (station.getLandstat_name_wmo()!=null) {
						stationName = station.getLandstat_name_wmo();
					}
					if ( station.getOrg_name()!=null) {
						orgName= station.getOrg_name();
						if (orgName.length()>2)
							orgName= orgName.substring(0,orgName.length()-1)+","+orgName.substring(orgName.length()-1);
						stationName+=" ("+orgName+")";
					}
				} //if station not null
				AlarmMod mod = alarm.getLatestAlarmMod();
				Integer state = (mod==null) ? -1 : mod.getAlarmState();

				//in which region? dispatch over tables
				boolean isNorthStation = NORTH_SET.contains(alarm.getLandstatId());
				boolean isSouthPlusBridgeStation = SOUTHPLUSBRIDGE_SET.contains(alarm.getLandstatId());
				ArrayList<CssTableModel> tables = new ArrayList<CssTableModel>();
				//if in both then check on the source of the warning from the 'author' field
				if (isNorthStation && isSouthPlusBridgeStation) {
					AlarmMod firstMod =alarm.getFirstAlarmMod();
					if ( firstMod !=null) {
						String firstAuthor = firstMod.getAuthor();
						if (("hsl noord").equalsIgnoreCase(firstAuthor) ) {
							isSouthPlusBridgeStation=false;
						}
						else if (("hsl zuid").equalsIgnoreCase(firstAuthor) || ("hsl brug").equalsIgnoreCase(firstAuthor) ) {
							isNorthStation=false;
						}
						//log.info("was in both, firstAuthor: "+firstAuthor+" now north = "+isNorthStation+"     south = "+isSouthStation);
					}
				} //if both north and south

				//if in none then warn and write in both
				if (!isNorthStation && !isSouthPlusBridgeStation) {
					log.warning("station ("+station.getLandstat_id()+") "+station.getLandstat_name_wmo() +" is not defined in either the NORTH or SOUTH set, now dispatched in both tables.");
					isNorthStation=true;
					isSouthPlusBridgeStation=true;
				}

				if (isNorthStation) {
					tables.add(alarmTableNorth);
					//add header if not yet done
					if (alarmTableNorth.getRowCount()<=1) {
						addHeader(alarmTableNorth,"regio noord");
					}
					Integer cnt = northAlarmCount.get(state);
					if (cnt==null)
						northAlarmCount.put(state,1);
					else
						northAlarmCount.put(state,cnt+1);
				}
				if (isSouthPlusBridgeStation) {
					tables.add(alarmTableSouth);
					//add header if not yet done
					if (alarmTableSouth.getRowCount()<=1) {
						addHeader(alarmTableSouth,"regio zuid");
					}
					Integer cnt = southAlarmCount.get(state);
					if (cnt==null)
						southAlarmCount.put(state,1);
					else
						southAlarmCount.put(state,cnt+1);
				}


				//count unique alarms
				Integer cnt = totalAlarmCount.get(state);
				if (cnt==null) 
					totalAlarmCount.put(state,1);
				else 
					totalAlarmCount.put(state,cnt+1);

				log.info(stationName+" north: "+isNorthStation+" ("+northAlarmCount.size()+")        south: "+isSouthPlusBridgeStation+" ("+southAlarmCount.size()+")     total ("+totalAlarmCount.size()+")");


				for (CssTableModel alarmTable: tables) {
					int newRow = alarmTable.addRow()-1;
					//mdlOpen.setRowId(newRow, mod);
					//alarmTable.setValueAt(mod.getAlarmState(),newRow,0);
					alarmTable.setValueAt(new DateCell(alarm.getDtg(),sdfMinute,"alaram_dtg"),newRow,1);
					alarmTable.setValueAt(stationName,newRow,2);
					alarmTable.setValueAt(alarm.getMessage(),newRow,3);

					if (mod!=null) {
						alarmTable.setValueAt(new StateCell(mod,"status_cell"),newRow,0);
						alarmTable.setValueAt(new CssCell(sdfMinute.format(mod.getDtg())+" ["+mod.getAuthor()+"] "+Display.getDefault(mod.getComment()) ),newRow,4);
					}
					else
						alarmTable.setValueAt(new StateCell(null,"status_cell"),newRow,0);
				} //for tables
			} //for alarms

			alarmTableSummary = createAlarmCountTable(northAlarmCount,southAlarmCount);
		} //if alarms > 0;

	}

	/**
	 * Creates a combine table of the North and South alarm counts
	 * @param northAlarmCount
	 * @param southAlarmCount
	 * @return
	 */
	private CssTableModel createAlarmCountTable(HashMap<Integer,Integer> northAlarmCount, HashMap<Integer,Integer> southAlarmCount) {
		log.info("northAlarmCount: "+northAlarmCount+"     southAlarmCount: "+southAlarmCount);


		CssTableModel mdl = new CssTableModel();
		mdl.setNrHeaderRows(0);

		mdl.setValueAt("north: ",0,0);
		mdl.setValueAt("south: ",1,0);
		//fill with blanks
		for (int i=1;i<=6;i++) {
			mdl.setValueAt("",0,i);
			mdl.setValueAt("",1,i);
		}

		int row = 0; 
		//isAlarm=false;
		//int openAlarmCount=0;

		ArrayList<HashMap<Integer,Integer>> alarmCounts = new ArrayList<HashMap<Integer,Integer>>();
		alarmCounts.add(northAlarmCount);
		alarmCounts.add(southAlarmCount);
		for (HashMap<Integer,Integer> alarmCount : alarmCounts) {
			int col = 1;
			//open
			Set<Integer> keySet = alarmCount.keySet();
			Integer openCount = alarmCount.get(AlarmMod.STATE_OPEN);

			if (openCount!=null) {
				mdl.setValueAt("<img src='"+StateCell.getIcon(AlarmMod.STATE_OPEN)+"'>", row, col++);
				mdl.setValueAt(openCount, row, col++);
				keySet.remove(AlarmMod.STATE_OPEN);
				//openAlarmCount+=openCount;
			}
			col=3;
			//surpressed
			Integer surpressedCount = alarmCount.get(AlarmMod.STATE_SURPRESSED);
			if (surpressedCount!=null) {
				mdl.setValueAt("<img src='"+StateCell.getIcon(AlarmMod.STATE_SURPRESSED)+"'>", row, col++);
				mdl.setValueAt(surpressedCount, row, col++);
				keySet.remove(AlarmMod.STATE_SURPRESSED);
			}
			col=5;
			//rest
			for (Integer status : keySet) {
				Integer count = alarmCount.get(status);
				mdl.setValueAt("<img src='"+StateCell.getIcon(status)+"'>", row, col++);
				mdl.setValueAt(count, row, col++);
			}
			row++;
		} //for alarmCounts
		return mdl;
	}

	/**
	 * adds the header to the alarmtable
	 * @param alarmTable
	 * @return
	 */
	private int addHeader(CssTableModel alarmTable, String header) {
		//init north table
		int row=0; int col=0;
		alarmTable.setNrHeaderRows(2);
		alarmTable.setValueAt(new HeaderCell(header,"val_col",5),row,col++);
		row++; col=0;
		alarmTable.setValueAt(new HeaderCell("","val_col"),row,col++);
		alarmTable.setValueAt(new HeaderCell("time","dtg_col"),row,col++);
		alarmTable.setValueAt(new HeaderCell("station","val_col"),row,col++);
		alarmTable.setValueAt(new HeaderCell("message","val_col"),row,col++);
		alarmTable.setValueAt(new HeaderCell("mod.","val_col"),row,col++);
		return row;
	}

	public CssTableModel getAlarmTableNorth() {
		return alarmTableNorth;
	}

	public CssTableModel getAlarmTableSouth() {
		return alarmTableSouth;
	}

	public CssTableModel getAlarmTableSummary() {
		return alarmTableSummary;
	}

	public HashMap<Integer,Integer> getTotalAlarmCount() {
		return totalAlarmCount;
	}

	//	public HashMap<Integer,Integer> getNorthAlarmCount() {
	//		return northAlarmCount;
	//	}
	//
	//	public HashMap<Integer,Integer> getSouthAlarmCount() {
	//		return southAlarmCount;
	//	}

	public DatePlusString getLastAlarmFase() {
		return lastAlarmFase;
	}

	/**
	 * Reads the latest alarmfase from file. This will allways be the LATEST alarmFase, 
	 * independent of lastObsStartDate and lastObsEndDate
	 * @return
	 */
	private void readLastAlarmFase() {
		lastAlarmFase = null;
		FileChannel fc = null;
		File file = null;
		try {
			file = new File(ALARM_FASE_PATH);
			RandomAccessFile raf = new RandomAccessFile(file, "r");
			fc = raf.getChannel();

			// Seek to the almost end of file
			raf.seek(Math.max(file.length()-512,0));

			String line = null;
			String lastLine = null;
			do {
				lastLine = line;
				line=raf.readLine();
			} while (line!=null);

			//parse the last line
			String sdfPattern = sdfAlarmFase.toPattern();
			if (lastLine!=null && lastLine.length()>sdfPattern.length()) {
				try {
					String ptDate = lastLine.substring(0, sdfPattern.length());
					String ptFase = lastLine.substring(sdfPattern.length());
					Date dtAlarmFase = sdfAlarmFase.parse(ptDate);
					lastAlarmFase = new DatePlusString(dtAlarmFase,ptFase);
				}
				catch (ParseException e) {
					e.printStackTrace();
				}
			}
		} //try
		catch (FileNotFoundException e) {
			log.severe("could not read the file: '" + file.getAbsolutePath() + "', msg: " + e.getMessage());
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		finally {
			try {
				if (fc != null)
					fc.close();
			}
			catch (IOException e) {
				e.printStackTrace();
			}
		}
		//log.warning("TESTEST: FAKE ALARMFASE. REMOVE!!!");
		//lastAlarmFase = new DatePlusString(new Date(),"alarmfase_160");

	}


	public void loadWarningState(Date obsStartDate, Date obsEndDate) {
		warningState = HslWarningState.getWarnings(obsStartDate, obsEndDate);	
		/*
 		log.warning("TESTEST: FAKE BAPPS WARNINGS. REMOVE!!!");
		Date now = new Date();
		 ArrayList<HslWarningState> flut = new  ArrayList<HslWarningState>();

		 HslWarningState h1 =new HslWarningState();
		 h1.setTrack(0);
		 h1.setWarningState(5);
		 h1.setFromDtg(new Date(now.getTime()-(3*DateTime.HOUR)));
		 h1.setUntilDtg(new Date(now.getTime()+(1*DateTime.HOUR)));
		 flut.add(h1);

		 HslWarningState h2 =new HslWarningState();
		 h2.setTrack(1);
		 h2.setWarningState(1);
		 h2.setFromDtg(new Date(now.getTime()-(2*DateTime.HOUR)));
		 h2.setUntilDtg(new Date(now.getTime()+(2*DateTime.HOUR)));
		 flut.add(h2);
		 warningState = flut;
		 */

	}

	public ArrayList<HslWarningState> getWarningState() {
		return warningState;
	}

	/**
	 * Puzzles out if the wwswarning has been notified in the BAPPS messages
	 * @return null if no wwswarning, true if it has been warned or false if it is not warned for
	 */
	private void refreshIsWarned() {
		Boolean bIsWarned = null;
		String message ="";
		//are the wws warnings communicated and in BAPPS?
		DatePlusString dpsAlarmFase = lastAlarmFase; //expected format alarmfase_300, alarmfase_220 etc..
		//
		if ( (dpsAlarmFase!=null) && (dpsAlarmFase.getValue()!=null) ) {
			Integer wwwAlarmSpeed = null;
			int idx = dpsAlarmFase.getValue().lastIndexOf('_');
			if (idx>0) {
				try {
					wwwAlarmSpeed = Integer.parseInt(dpsAlarmFase.getValue().substring(idx+1));
				}
				catch (NumberFormatException ex) {
					log.warning("Could not parse '"+dpsAlarmFase.getValue()+"' to a speed factor, expected format 'alarmfase_300, alarmfase_220 etc..'");
				}
			} //idx>0

			//log.info("in refreshIsWarned, dpsAlarmFase: "+dpsAlarmFase+"   wwwAlarmSpeed: "+wwwAlarmSpeed);

			//if the wws has a speed defined which is lower than normal
			if (wwwAlarmSpeed!=null && ( wwwAlarmSpeed<AlarmLimit.NORMAL.kmh() || (wwwAlarmSpeed==App.DEGRADED_MODE ))  ) {
				bIsWarned = Boolean.FALSE;
				//get the worst Bapps warning
				AlarmLimit worstBappsWarning = null;
				for (HslWarningState warning: warningState) {
					AlarmLimit bappsLim = App.getAlarmLimitFromHslWarningState(warning.getWarningState());
					if (bappsLim!=null) {
						if (worstBappsWarning==null) {
							worstBappsWarning = bappsLim;
						}
						else if (worstBappsWarning.kmh()>=bappsLim.kmh()) {
							worstBappsWarning = bappsLim;
						}
					}
				} //for warnings
				message = "NO BAPPS COMMUNICATION!";
				if (worstBappsWarning!=null) {
					if (wwwAlarmSpeed < worstBappsWarning.kmh() ) {
						message = "WARNING! BAPPS warned for "+worstBappsWarning.kmh()+" but WWS says "+wwwAlarmSpeed+"!";
					}
					else if (wwwAlarmSpeed == worstBappsWarning.kmh() ) {
						message = "WWS is "+wwwAlarmSpeed+" and BAPPS warned for "+worstBappsWarning.kmh();
						bIsWarned = Boolean.TRUE;
					}
					else if (wwwAlarmSpeed > worstBappsWarning.kmh() ) {
						message = "BAPPS warned for "+worstBappsWarning.kmh()+" but WWS says "+wwwAlarmSpeed+"!";
						bIsWarned = Boolean.TRUE;
					}	
				} // if worstBappsWarning not null
			} //wwwAlarmSpeed not null

		} //dps not null

		isWarned = bIsWarned;
		isWarnedMessage = message;
		//log.info("uit refreshIsWarned, isWarned: "+isWarned+" message: "+isWarnedMessage);
	}

	public Boolean isWarned() {
		return isWarned;
	}

	public String getIsWarnedMessage() {
		return isWarnedMessage;
	}


	//	----  other ------------

	public void setTimeZone(TimeZone timeZone) {
		if ( (timeZone!=null) && (!timeZone.equals(this.timeZone)) ) {
			this.timeZone = timeZone;
			cal.setTimeZone(timeZone);
			sdfMinute.setTimeZone(timeZone);
		}
	}





} //end class
