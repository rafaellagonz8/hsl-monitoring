package meteoconsult.products.hsl.table;

import meteoconsult.lib.common.table.cell.ColSpannable;
import meteoconsult.lib.common.table.cell.CssCell;


public class HeaderCell extends CssCell implements ColSpannable {
	private int colspan =1;
	
	public HeaderCell(Object value,String cssClass) {
		super(value,cssClass);
	}
	
	public int getColSpan() {
		return colspan;
	}

	public void setColSpan(int colspan) {
		this.colspan=colspan;
	}

}
