package meteoconsult.products.hsl.table;

import meteoconsult.lib.common.table.cell.CssCell;
import meteoconsult.products.hsl.App;

public class WindDirCell extends CssCell {
	
	public WindDirCell(Object dd) {
		setValue(dd);
	}
	
	/**
	 * @param value the value to set
	 */
	public void setValue(Object value) {
		super.setValue(value);
		setCssClass(App.ddCssClass(value));
	}

}
