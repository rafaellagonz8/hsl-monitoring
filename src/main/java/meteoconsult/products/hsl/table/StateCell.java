package meteoconsult.products.hsl.table;

import java.util.LinkedHashSet;

import meteoconsult.database.tables.rbsadmin.Alarm;
import meteoconsult.database.tables.rbsadmin.AlarmMod;
import meteoconsult.lib.common.table.cell.CssCell;
import meteoconsult.products.hsl.App;


public class StateCell extends CssCell {
	
	public StateCell(Object value,String cssClass) {
		super(value,cssClass);
	}
	
	
	public String getHtml() {
		String icon = App.ICON_BLANK;
		if ( (getValue()!=null) && (getValue() instanceof AlarmMod) ) {
			AlarmMod mod = (AlarmMod)getValue();
			int state = mod.getAlarmState();
			if (state==AlarmMod.STATE_OPEN) {
				icon=App.ICON_OPEN;
				//check beforelast
				Alarm alarm = mod.getAlarm();
				if (alarm!=null) {
					LinkedHashSet<AlarmMod> mods = alarm.getAlarmMods();
					if (mods.size()>1) {
						int beforeLast = mods.size()-2;
						int i=0;
						for (AlarmMod tmpMod : mods) {
							if (i==beforeLast) {
								switch (tmpMod.getAlarmState()) {
									case AlarmMod.STATE_REX: { 
										icon=App.ICON_REX_OPEN;
									} break;
									case AlarmMod.STATE_SURPRESSED: { 
										icon=App.ICON_SURPRESSED_OPEN;
									} break;
									case AlarmMod.STATE_HANDLED: { 
										icon=App.ICON_HANDLED_OPEN;
									} break;
								} //switch
								break;
							} //beforelast
							i++;
						} //for mods
					} //mods size>1
				} //if alarm not null
			} 
			else
				icon = StateCell.getIcon(state);
		} //value is integer
		return "<img src='"+icon+"'>";
	}
	
	/**
	 * Returns the icon for the current state
	 * @param state
	 * @return
	 */
	public static String getIcon(int state) {
		String icon=App.ICON_BLANK;
		switch (state) {
			case AlarmMod.STATE_OPEN: { 
				icon=App.ICON_OPEN;
			} break;
			case AlarmMod.STATE_REX: { 
				icon=App.ICON_REX;
			} break;
			case AlarmMod.STATE_SURPRESSED: { 
				icon=App.ICON_SURPRESSED;
			} break;
			case AlarmMod.STATE_HANDLED: { 
				icon=App.ICON_HANDLED;
			} break;
		} //switch
		return icon;
	}
}
