package meteoconsult.products.hsl.table;

import meteoconsult.lib.common.table.cell.CssCell;
import meteoconsult.products.hsl.App;
import meteoconsult.products.hsl.App.AlarmLimit;
import meteoconsult.products.hsl.limit.AlarmLimitValue;
import meteoconsult.util.Display;

public class WindCell extends CssCell {
	
	Integer colorCode = null;
	
	public WindCell(Integer stationId, Number f, Number dd) {
		setValue(f);
		if ( f!=null ) {
			AlarmLimitValue limitValue = App.getWorstLimit(stationId, f, dd);
			if (limitValue!=null)
				colorCode = limitValue.getLimit().colorCode();
		}
	}
	
	/**
	 * @param value the value to set
	 */
	public void setValue(Object value) {
		super.setValue(value);
	}

	public String getHtml() {
		if (colorCode==null) {
			return Display.getDefault(getValue());
		}
		else {
			return "<span style='background-color:#"+Integer.toHexString(colorCode)+"'>"+Display.getDefault(getValue())+"</span>";
		}
	}

}
