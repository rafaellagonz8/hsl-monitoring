package meteoconsult.products.hsl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Set;
import java.util.TimeZone;
import java.util.logging.Logger;

import meteoconsult.database.tables.rbsadmin.AlarmMod;
import meteoconsult.database.tables.rbsadmin.HslWarningState;
import meteoconsult.lib.common.table.CssTableModel;
import meteoconsult.lib.common.table.MdlToHtml;
import meteoconsult.products.hsl.App.AlarmLimit;
import meteoconsult.products.hsl.table.StateCell;
import meteoconsult.util.DatePlusString;
import meteoconsult.util.DateTime;

/**
 * Bean for all graphs and data on the left side
 */
public class DataPageBean {
	public final static Logger log = Logger.getLogger("meteoconsult.products.hsl");
	
	private TimeZone timeZone;
	private HslDataProvider dataProvider;
	private SimpleDateFormat sdfHour = new SimpleDateFormat("HH:mm");
	private SimpleDateFormat sdfWeek = new SimpleDateFormat("EE HH:mm");
	private SimpleDateFormat sdfMonth = new SimpleDateFormat("dd MMM HH:mm");
	
	//private Integer openAlarmCount = null;//counter for the number of valid open database Alarms
	
	private MdlToHtml mdlToHtml = new MdlToHtml();

	public String getWindTable() {
		if (dataProvider==null) {
			return "not ready";
		}
		else {
			CssTableModel mdl = dataProvider.getVarTableMdl();
			if (mdl==null) return "not ready";
			else
				return mdlToHtml.getTable(mdl,"id='minute_table'");
		}
	}


	/**
	 * Returns a table with ff and fxMax for the hsl stations
	 *
	 */
	public String getMaxWindBridgeTable() {
		if (dataProvider==null) {
			return "not ready";
		}
		else {
			return getMaxWindTable(dataProvider.getMaxBridgeFf(), dataProvider.getMaxBridgeFx());
		}
	}

	/**
	 * Returns a table with ff and fxMax for the hsl stations
	 *
	 */
	public String getMaxWind1MinTable() {
		if (dataProvider==null) {
			return "not ready";
		}
		else {
			return getMaxWindTable(dataProvider.getMax1MinFf(), dataProvider.getMax1MinFx());
		}
	}

	/**
	 * Returns a table with ff and fxMax for the hsl stations
	 *
	 */
	public String getMaxWind10MinTable() {
		if (dataProvider==null) {
			return "not ready";
		}
		else {
			return getMaxWindTable(dataProvider.getMax10MinFf(), dataProvider.getMax10MinFx());
		}
	}

	/**
	 * Returns a table with ff and fxMax for the hsl stations
	 *
	 */
	private String getMaxWindTable(HslData maxFfData, HslData maxFxData) {	
		StringBuffer sb = new StringBuffer();
		sb.append("<table><tr>");

		if (maxFfData==null) 
			sb.append("  <td class='digit unknown_wind'>?</td>");
		else {
			String styleFF = "style='background-color:#"+Integer.toHexString(maxFfData.getFfColorCode())+"'";
			sb.append("  <td class='digit'"+styleFF+" title='ff="+maxFfData.getFf()+" ["+maxFfData.getStatId()+"] "+maxFfData.getName()+" ("+sdfHour.format(maxFfData.getDtg())+"u "+maxFfData.getDd()+"&deg;)'>"
					+maxFfData.getFf()+"</td>");
		}
		if (maxFxData==null)
			sb.append("  <td class='digit unkown_wind'>?</td>");
		else {
			String styleFX = "style='background-color:#"+Integer.toHexString(maxFfData.getFxColorCode())+"'";
			sb.append("  <td class='digit'"+styleFX+" title='fx="+maxFxData.getFx()+" ["+maxFxData.getStatId()+"] "+maxFxData.getName()+" ("+sdfHour.format(maxFxData.getDtg())+"u "+maxFxData.getDd()+"&deg;)'>"
					+maxFxData.getFx()+"</td>");
		}
		sb.append("</tr></table>");
		return sb.toString();
	}


	public String getActualWindBridgeTable() {
		if (dataProvider==null) {
			return "not ready";
		}
		else {
			return getActualWindTable(dataProvider.getActualBridgeF());
		}
	}


	/**
	 * Returns a table with the latest ff and fx for the bridge station
	 *
	 */
	private String getActualWindTable(HslData actualData) {
		StringBuffer sb = new StringBuffer();

		if ( (actualData==null) || (actualData.getDtg()==null) ) {
			sb.append("<table>");
			sb.append("<tr><td colspan=2>Brug Holl. Diep</td></tr>");
			sb.append("<tr><td colspan=2>actueel</td></tr>");
			sb.append("<tr>");
			sb.append("  <td class='digit unknown_wind'>?</td>");
			sb.append("  <td class='digit unknown_wind'>?</td>");
			sb.append("</tr>");
			sb.append("</table>");
		}
		else {
			long delta = (new Date().getTime() - actualData.getDtg().getTime()) /DateTime.MINUTE;
			String timeOutClass= (delta>15) ? " class='timed_out'": "";
			String sDtg = sdfHour.format(actualData.getDtg())+" u " ;
			sb.append("<table"+timeOutClass+">");
			sb.append("<tr><td colspan=2>Brug Holl. Diep</td></tr>");
			sb.append("<tr><td colspan=2>"+sDtg+"</td></tr>");
			sb.append("<tr>");
			
			String styleFF = "style='background-color:#"+Integer.toHexString(actualData.getFfColorCode())+"'";
			sb.append("  <td class='digit'"+styleFF+" title='ff="+actualData.getFf()+" ["+actualData.getStatId()+"] "+actualData.getName()+" ("+sDtg+actualData.getDd()+"&deg;)'>"
					+actualData.getFf()+"</td>");

			String styleFX = "style='background-color:#"+Integer.toHexString(actualData.getFxColorCode())+"'";
			sb.append("  <td class='digit'"+styleFX+" title='fx="+actualData.getFx()+" ["+actualData.getStatId()+"] "+actualData.getName()+" ("+sDtg+actualData.getDd()+"&deg;)'>"
					+actualData.getFx()+"</td>");
			
			sb.append("</tr>");
			sb.append("</table>");
		}

		return sb.toString();
	}


	public String getNorthAlarms() {
		if (dataProvider==null) return "not ready";
		
		CssTableModel mdlNorth = dataProvider.getAlarmTableNorth();
		if (mdlNorth==null) return "not ready";
		else {
			if (mdlNorth.getColumnCount()>0) {
				return mdlToHtml.getTable(mdlNorth,"class='alarm_table'");
			}
			else {
				return "<center>- no alarms -</center>";
			}
		} //else
	}
	
	public String getSouthAlarms() {
		if (dataProvider==null) return "not ready";
		
		CssTableModel mdlSouth = dataProvider.getAlarmTableSouth();
		if (mdlSouth==null) return "not ready";
		else {
			//App.log.info("rowcount: "+mdl.getRowCount()+"   colcount: "+mdl.getColumnCount() );
			if (mdlSouth.getColumnCount()>0) {
				return mdlToHtml.getTable(mdlSouth,"class='alarm_table'");
			}
			else {
				return "<center>- no alarms -</center>";
			}
		} //else
	}
	
	/**
	 * Returns the latest alarm fase as descripted in the queue on churada
	 * @return
	 */
	public String getLatestAlarmFase() {
		if (dataProvider==null) 
			return "not ready";
		else {
			String alarmFase;
			String css;
			DatePlusString dpsAlarmFase = dataProvider.getLastAlarmFase();
			if (dpsAlarmFase==null) {
				alarmFase = "- alarmfase onbekend -";
				//css="class='warning'";
				css="class='warning_gray'"; //TODO : enable after alarm can be retrieved from Tramontana
			}
			else {
				alarmFase = format(dpsAlarmFase.getDate())+" "+dpsAlarmFase.getValue();
				if ("alarmfase_300".equalsIgnoreCase(dpsAlarmFase.getValue()))
					css="";
				else if ("alarmfase_990".equalsIgnoreCase(dpsAlarmFase.getValue()))
					css="class='warning_orange'";
				else
					css="class='warning'";
			}
			return "<span "+css+">"+alarmFase+"</span>";
		}
	}
	
	/**
	 * Returns the warnings as submitted by the BAPPS application
	 * @return
	 */
	public String getBappsWarnings() {
		if (dataProvider==null) 
			return "not ready";
		else {
			ArrayList<HslWarningState> warnings = dataProvider.getWarningState();
			StringBuffer sbWarnings = new StringBuffer();
			if (warnings==null || warnings.size()==0) {
				sbWarnings.append("no BAPPS warnings");
			}
			else {
				for (HslWarningState warning: warnings) {
					String spanClass="warning";
					Integer state = warning.getWarningState();
					if (state!=null && (state.intValue()==HslWarningState.FASE_300) ) {
						spanClass = "no_warning";
					}
					sbWarnings.append("<span class='"+spanClass+"'>");
					AlarmLimit limit = App.getAlarmLimitFromHslWarningState(warning.getWarningState());
					
					String warningName = (limit==null) ? "unknown HslWarningState <"+warning.getWarningState()+">" : limit.name();
					sbWarnings.append(getTrackName(warning.getTrack())+"&nbsp;"+format(warning.getFromDtg())+" - "+format(warning.getUntilDtg())+" "+warningName+"<br/>");
					sbWarnings.append("</span>");
				}
			}
			return sbWarnings.toString();
		}
	}
	
	
	
	/**
	 * 
	 * @return
	 */
	public String getAlarmStatusSummary() {
		if (dataProvider==null) 
			return "not ready";
		else {
			HashMap<Integer,Integer> totalAlarmCount = dataProvider.getTotalAlarmCount();
			if (totalAlarmCount==null) {
				return "not ready";
			}
			else if (totalAlarmCount.size()==0) {
				return "no HSL alarms";
			}
			else {
				//CssTableModel alarmCount = createAlarmCountTable(dataProvider.getNorthAlarmCount(),dataProvider.getSouthAlarmCount());
				CssTableModel alarmCount = dataProvider.getAlarmTableSummary();
				return mdlToHtml.getTable(alarmCount,"class='alarm_summary_table'");
			} //count >0
		} //else
	}
	
	
	/**
	 * Uses different format for less than 24 hour ago
	 * @param dt
	 * @return
	 */
	private String format(Date dt) { 
		if (dt==null) 
			return "-";
		
		SimpleDateFormat sdf = sdfWeek;
		if (dataProvider!=null && dataProvider.getRefreshTime()!=null && dt!=null) {
			Date refreshTime = dataProvider.getRefreshTime();
			long lDelta = Math.abs(dt.getTime()-refreshTime.getTime());
			if (lDelta<DateTime.DAY) {
				sdf = sdfHour;
			}
			else if (lDelta<DateTime.WEEK) {
				sdf = sdfWeek;
			}
			else {
				sdf = sdfMonth;
			}
		}
		return sdf.format(dt);
	}
	

	//---- initiation methods ----

	public TimeZone getTimeZone() {
		return timeZone;
	}


	public void setTimeZone(TimeZone timeZone) {
		if ( (timeZone!=null) && (!timeZone.equals(this.timeZone)) ) {
			this.timeZone = timeZone;
			sdfHour.setTimeZone(timeZone);
			sdfWeek.setTimeZone(timeZone);
			sdfMonth.setTimeZone(timeZone);
			//App.log.info("sdfHour naar tijdzone: "+sdfHour.getTimeZone());
		}
	}


	public void setHslDataProvider(HslDataProvider dataProvider) {
		this.dataProvider = dataProvider;
	}

	/**
	 * Returns the .wav to beused for the current alarm or either null
	 * @return
	 */
	public String getAlarmSound() {
		if (dataProvider==null) {
			return null;
		}
		String wavFile = null;
		
		//do a sound if there are open alarms or if a wws alarm is not warned for
		boolean notWarnedFor = Boolean.FALSE.equals(dataProvider.isWarned());
		
		HashMap<Integer,Integer> totalAlarmCount = dataProvider.getTotalAlarmCount();
		Integer openAlarmCount = totalAlarmCount.get(AlarmMod.STATE_OPEN);
		
		if ( (openAlarmCount!=null && openAlarmCount>0) || notWarnedFor ) {
				//without the extension, it will automatically make 2 entries with both .wav an .mp3
				//wavFile = "wav/steamwhistle";
				wavFile = null; //RL: switched all sound off 8 July 2014
		} 
		return wavFile;
	}
	
	public String getAlarmMesssage() {
		if (dataProvider==null) {
			return "not ready";
		}
		else return dataProvider.getIsWarnedMessage();
	}
	
	
	private String getTrackName(int track) {
		String result;
		switch (track) {
			case HslWarningState.TRACK_NORTH:
				result = "noord";
				break;
			case HslWarningState.TRACK_SOUTH:
				result = "zuid";
				break;
			default:
				result = "unknown track ("+track+")";
		}
		return result;		
	}
	
//	private String getWarningStateName(int state) {
//		String result;
//		switch (state) {
//			case HslWarningState.FASE_160:
//				result = "160 gewaarschuwd";
//				break;
//			case HslWarningState.FASE_000:
//				result = "STOP gewaarschuwd";
//				break;
//			default:
//				result = "onbekende waarschuwing ("+state+")";
//		}
//		return result;		
//	}
	
	
	
	public String getLegendTable() {
		if (dataProvider==null) {
			return "not ready";
		}
		else {
			CssTableModel mdl = dataProvider.getLegendTableMdl();
			if (mdl==null) return "not ready";
			else
				return mdlToHtml.getTable(mdl,"id='legend_table'");
		}
	}
	
	public String getSelectedStationName() {
		if (dataProvider==null) {
			return "not ready";
		}
		else {
			return dataProvider.getSelectedStationName();
		}
	}
	
	public String getBridgeStationName() {
		if (dataProvider==null) {
			return "not ready";
		}
		else {
			return dataProvider.getBridgeStationName();
		}
	}
	
	

} //end class
